# CONFIG-MS
## Descripción

Este repositorio contiene los ficheros de configuración de los entornos STECMA.
Se compone de 4 secciones:

* config
* consul
* db
* docker

## config

Para la integración contínua del proyecto se utiliza jenkins, en esta carpeta se guardan los pipelines de cada uno de los proyectos.
El despliegue de las aplicaciones está contenido en openshift, para cada uno de los servicios se guarda la configuración en su carpeta correspondiente.

## consul

Para la gestión de las propiedades de configuración de los proyectos se usa Consul KV (https://www.consul.io/docs/dynamic-app-config/kv).
Esta carpeta contiene 2 ficheros:

* consul-keys.json
* consul-keys-decodificadas.json

### consul-keys.json
Son los datos obtenidos de consul directamente, el valor de cada clave está codificada en base64.

### consul-keys-decodificadas.json
Son los datos de consul sin codificar en base64 para poder ser legibles al ojo humano y poder consultarlos si es necesario.


## db
Contiene todos los scripts de base de datos necesarios en la aplicación (DDL,DML,DCL)

## docker
Para montar un entorno de desarrollo local independiente de los servicios existentes en los distintos entornos tenemos la posibilidad de montarnos nuestra estructura de microservicios en nuestro maquina local, para ello necesitamos tener instalado docker en nuestro equipo.
Esta carpeta se compone de:

* dockerfiles
* temp
* crearImagen.bat
* docker-compose.yml

### dockerfiles
Son cada uno de los ficheros de generación de las imagenes de docker en local para cada uno de los microservicios.

### temp
Carpeta en la que se almacenarán los ficheros temporales necesarios a la hora de la compilación de los proyectos y la generación de las imágenes, se borran tras cada ejecución.

### crearImagen.bat
Archivo ejecutable que gestionará mediante opciones de selección la versión y la imagen que queremos ejecutar.
Cuando se descarguen los proyectos será necesario editar este fichero mediante un programa de edición de texto para configurar la ubicación de cada uno de los proyectos, las propiedades a editar son: "workspace_location" y "docker_file_location".

Estos son los pasos a seguir:

1. Selección de versión.
2. Selección de la imagen que queremos generar.
3. Una vez creada la imagen y pulsemos una tecla para continuar volveremos al menú inicial para poder generar otra imagen o salir.

IMPORTANTE: La primera vez será necesario generar todas las imágenes de los microservicios para poder levantar en local correctamente el ecosistema.

### docker-compose
En esta carpeta existe un docker-compose en el que están definidas todas los componentes necesarios para levantar el entorno en local.

Contiene una imagen de redis, una zookeeper y una de kafka que son necesarias, además de las propias generadas en el paso anterior.

Es necesario configurar en el fichero en el servicio del proxy (proxy-ms) nuestra ip en la propiedad:

extra_hosts:
- "local:NUESTRA_IP"  

Para que el proxy la resuelva correctamente.
Y poner en los servicios la versión de la imagen que nos hemos generado (por defecto tienen la 1.0.0).

Si quisiéramos deshabilitar cualquiera de los servicios y ejecutarlos desde nuestro IDE para debugar, sobraría con comentar el servicio deseado, y configurar el puerto correcto en el IDE si los servicios son los siguientes:
* proxy-ms: 8080
* login-ms: 8082
* gestcom-ms: 8083

Para login-data-ms y gestcom-data-ms no es necesario ya que las peticiones se gestionan a través de kafka.

Para levantar todos los servicios se debe ejecutar el siguiente comando tomando como raíz la carpeta "docker"":

```bash prompt> docker-compose up -d```



