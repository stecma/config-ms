DROP TABLE NWTMNU.MNU_LOGIN_MASTER CASCADE CONSTRAINTS;

CREATE TABLE NWTMNU.MNU_LOGIN_MASTER
(
  MLM_ID       VARCHAR2(3 BYTE)                 NOT NULL,
  MLM_TYPE     VARCHAR2(20 BYTE)                NOT NULL,
  MLM_CREATED  DATE                             DEFAULT SYSDATE               NOT NULL,
  MLM_UPDATED  DATE                             DEFAULT SYSDATE               NOT NULL,
  MLM_JSON     VARCHAR2(2000 BYTE)              NOT NULL
)
TABLESPACE NEWTOUR_DAT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE;

COMMENT ON COLUMN NWTMNU.MNU_LOGIN_MASTER.MLM_ID IS 'Identificador de maestro';

COMMENT ON COLUMN NWTMNU.MNU_LOGIN_MASTER.MLM_TYPE IS 'Tipo de maestro';


CREATE UNIQUE INDEX NWTMNU.MLM_UK ON NWTMNU.MNU_LOGIN_MASTER
(MLM_ID, MLM_TYPE)
LOGGING
TABLESPACE NEWTOUR_IND
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE NWTMNU.MNU_LOGIN_MASTER ADD (
  CONSTRAINT MLM_UK
  UNIQUE (MLM_ID, MLM_TYPE)
  USING INDEX NWTMNU.MLM_UK
  ENABLE VALIDATE);


CREATE OR REPLACE PUBLIC SYNONYM MNU_LOGIN_MASTER FOR NWTMNU.MNU_LOGIN_MASTER;


GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON NWTMNU.MNU_LOGIN_MASTER TO PUBLIC;
