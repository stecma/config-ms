SET DEFINE OFF;
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('MKV', 'COMPANY', TO_DATE('03/01/2020 11:53:16', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:43:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"MKV","name":[{"key":"ES","value":"Travelplan US"},{"key":"EN","value":"Travelplan US"},{"key":"PT","value":"Travelplan US"}],"act":false,"del":false,"entity":"COMPANY","logo":"travelplan.jpg","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('WIS', 'COMPANY', TO_DATE('03/01/2020 11:53:16', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:43:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"WIS","name":[{"key":"ES","value":"Welcome Incoming Services"},{"key":"EN","value":"Welcome Incoming Services"},{"key":"PT","value":"Welcome Incoming Services"}],"act":false,"del":false,"entity":"COMPANY","logo":"wis.jpg","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('BED', 'BUSINESS_APP', TO_DATE('15/07/2020 10:53:06', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:44', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"BED","name":[{"key":"ES","value":"BedBank"},{"key":"EN","value":"BedBank"},{"key":"PT","value":"BedBank"}],"act":true,"del":false,"entity":"BUSINESS_APP","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"COMPANY","value":["WIS"]}]}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('IBT', 'COMPANY', TO_DATE('03/01/2020 11:53:17', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:43:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"IBT","name":[{"key":"ES","value":"Iberotours"},{"key":"EN","value":"Iberotours"},{"key":"PT","value":"Iberotours"}],"act":false,"del":false,"entity":"COMPANY","logo":"touring.jpg","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('TVP', 'COMPANY', TO_DATE('03/01/2020 11:53:17', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:43:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"TVP","name":[{"key":"ES","value":"Travelplan SAU"},{"key":"EN","value":"Travelplan SAU"},{"key":"PT","value":"Travelplan SAU"}],"act":false,"del":false,"entity":"COMPANY","logo":"travelplan.jpg","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('PRD', 'ENVIRONMENT', TO_DATE('03/01/2020 11:53:17', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:39', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"PRD","name":[{"key":"ES","value":"Producci�n"},{"key":"EN","value":"Production"},{"key":"PT","value":"Produ��o"}],"act":false,"del":false,"entity":"ENVIRONMENT","host":"http://proxy-ms-travelplan-des.appsdes.devops.plexus.local","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('PRE', 'ENVIRONMENT', TO_DATE('03/01/2020 11:53:17', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:39', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"PRE","name":[{"key":"ES","value":"Pre Producci�n"},{"key":"EN","value":"Preproduction"},{"key":"PT","value":"Pr�-produ��o"}],"act":false,"del":false,"entity":"ENVIRONMENT","host":"http://proxy-ms-travelplan-des.appsdes.devops.plexus.local","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('DEV', 'ENVIRONMENT', TO_DATE('03/01/2020 11:53:18', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:39', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"DEV","name":[{"key":"ES","value":"Desarrollo"},{"key":"EN","value":"Development"},{"key":"PT","value":"Desenvolvimento"}],"act":false,"del":false,"entity":"ENVIRONMENT","host":"http://proxy-ms-travelplan-des.appsdes.devops.plexus.local","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('EN', 'LANGUAGES_APP', TO_DATE('03/01/2020 11:53:17', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:43:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"EN","name":[{"key":"ES","value":"Ingl�s"},{"key":"EN","value":"English"},{"key":"PT","value":"Ingl�s"}],"act":false,"del":false,"entity":"LANGUAGES_APP","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('ES', 'LANGUAGES_APP', TO_DATE('03/01/2020 11:53:17', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:43:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"ES","name":[{"key":"ES","value":"Espa�ol"},{"key":"EN","value":"Spanish"},{"key":"PT","value":"Espanhol"}],"act":false,"del":false,"entity":"LANGUAGES_APP","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('PT', 'LANGUAGES_APP', TO_DATE('03/01/2020 11:53:16', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:43:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"PT","name":[{"key":"ES","value":"Portugu�s"},{"key":"EN","value":"Portuguese"},{"key":"PT","value":"Portugu�s"}],"act":false,"del":false,"entity":"LANGUAGES_APP","test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('REC', 'BUSINESS_APP', TO_DATE('15/07/2020 10:53:06', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:45', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"REC","name":[{"key":"ES","value":"Receptivo"},{"key":"EN","value":"Service destination"},{"key":"PT","value":"Receptivo"}],"act":true,"del":false,"entity":"BUSINESS_APP","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"COMPANY","value":["WIS"]}]}');
Insert into NWTMNU.MNU_LOGIN_MASTER
   (MLM_ID, MLM_TYPE, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   ('TOP', 'BUSINESS_APP', TO_DATE('15/07/2020 10:53:06', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:45', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"TOP","name":[{"key":"ES","value":"Tour Operations"},{"key":"EN","value":"Tour Operations"},{"key":"PT","value":"Tour Operations"}],"act":  true,"del":  false,"entity":"BUSINESS_APP","test":  false,"demo":  false,"corp":  false,"supplier":  false,"own":  false,"codes":[{"key":"COMPANY","value":["MKV","IBT","TVP"]}]}');
COMMIT;
