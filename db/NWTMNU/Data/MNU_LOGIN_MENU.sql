SET DEFINE OFF;
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (22, TO_DATE('13/01/2020 14:48:49', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:58:01', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"22","name":[{"key":"EN","value":"Administration"},{"key":"ES","value":"Administración"},{"key":"PT","value":"Administração"}],"act":true,"del":false,"icon":"settings","codes":[{"key":"groups","value":["45"]},{"key":"services","value":["55","60"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (41, 'PRD', TO_DATE('25/02/2020 13:10:35', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:58:06', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"41","name":[{"key":"PT","value":"BI"},{"key":"EN","value":"BI"},{"key":"ES","value":"BI"}],"act":true,"del":false,"env":"PRD","icon":"equalizer","grp":[],"codes":[{"key":"services","value":["95"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (44, 'PRD', TO_DATE('25/02/2020 15:49:20', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:58:11', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"44","name":[{"key":"PT","value":"BackOffice"},{"key":"EN","value":"BackOffice"},{"key":"ES","value":"BackOffice"}],"act":true,"del":false,"env":"PRD","icon":"palette","grp":[],"codes":[{"key":"services","value":["99","95","101","102","100","103"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (45, 'DEV', TO_DATE('25/02/2020 18:01:14', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:56:17', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"45","name":[{"key":"PT","value":"Operacional"},{"key":"EN","value":"Operational"},{"key":"ES","value":"Operativa"}],"act":true,"del":false,"env":"DEV","icon":"eco","grp":[],"codes":[{"key":"services","value":["63","106","105"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (46, 'DEV', TO_DATE('25/02/2020 20:59:01', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:56:23', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"46","name":[{"key":"PT","value":"Receptivo"},{"key":"EN","value":"Receptive"},{"key":"ES","value":"Receptivo"}],"act":true,"del":false,"env":"DEV","icon":"business","grp":[],"codes":[{"key":"services","value":["110"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (179, TO_DATE('20/10/2020 7:58:37', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('20/10/2020 7:59:24', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"179","name":[{"key":"PT","value":"test"},{"key":"ES","value":"test"},{"key":"EN","value":"test"}],"act":false,"del":true}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (62, 'PRE', 'TVP', TO_DATE('26/02/2020 12:56:45', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 11:02:04', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"62","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRE","icon":"euro","cia":"TVP","codes":[{"key":"groups","value":["105"]},{"key":"services","value":["123"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (42, 'DEV', TO_DATE('25/02/2020 14:37:34', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:56:11', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"42","name":[{"key":"PT","value":"BackOffice"},{"key":"EN","value":"BackOffice"},{"key":"ES","value":"BackOffice"}],"act":true,"del":false,"env":"DEV","icon":"palette","grp":[],"codes":[{"key":"services","value":["62","64","208"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (43, 'PRE', TO_DATE('25/02/2020 14:48:47', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:57:13', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"43","name":[{"key":"PT","value":"BackOffice"},{"key":"EN","value":"BackOffice"},{"key":"ES","value":"BackOffice"}],"act":true,"del":false,"env":"PRE","icon":"palette","grp":[],"codes":[{"key":"services","value":["96","97","98"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (63, 'PRE', 'MKV', TO_DATE('26/02/2020 12:57:19', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 11:02:39', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"63","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRE","icon":"euro","cia":"MKV","codes":[{"key":"groups","value":["105"]},{"key":"services","value":["123"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (65, 'PRE', 'WIS', TO_DATE('26/02/2020 13:05:11', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:57:27', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"65","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRE","icon":"euro","cia":"WIS","grp":[],"codes":[{"key":"services","value":["124","123"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (61, 'DEV', TO_DATE('26/02/2020 11:21:35', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:56:28', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"61","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"DEV","icon":"euro","grp":[],"codes":[{"key":"services","value":["119","118"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (47, 'PRE', TO_DATE('26/02/2020 8:32:00', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:57:17', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"47","name":[{"key":"PT","value":"Receptivo"},{"key":"EN","value":"Receptive"},{"key":"ES","value":"Receptivo"}],"act":true,"del":false,"env":"PRE","icon":"business","grp":[],"codes":[{"key":"services","value":["111"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (48, 'PRE', TO_DATE('26/02/2020 8:42:06', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:57:22', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"48","name":[{"key":"PT","value":"Operacional"},{"key":"EN","value":"Operational"},{"key":"ES","value":"Operativa"}],"act":true,"del":false,"env":"PRE","icon":"eco","grp":[],"codes":[{"key":"services","value":["112","113","114"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (49, 'PRD', TO_DATE('26/02/2020 8:50:21', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:58:17', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"49","name":[{"key":"PT","value":"Operacional"},{"key":"EN","value":"Operational"},{"key":"ES","value":"Operativa"}],"act":true,"del":false,"env":"PRD","icon":"eco","grp":[],"codes":[{"key":"services","value":["115","116"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (64, 'PRE', 'IBT', TO_DATE('26/02/2020 13:03:03', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:44', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"64","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRE","icon":"euro","cia":"IBT","srv":[{"id":"123","name":[{"key":"PT","value":"Call Center"},{"key":"EN","value":"Call Center"},{"key":"ES","value":"Call Center"}],"act":true,"del":false,"env":"PRE","icon":"headset_mic","url":{"url":"http://prenwtweb.travelplan.es/","params":[],"readonly":false},"ext":true,"fie":false}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (67, 'PRD', 'MKV', TO_DATE('26/02/2020 13:17:49', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 11:04:18', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"67","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRD","icon":"euro","cia":"MKV","codes":[{"key":"groups","value":["107","106"]},{"key":"services","value":["128"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (69, 'PRD', 'WIS', TO_DATE('26/02/2020 13:21:40', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:58:28', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"69","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRD","icon":"euro","cia":"WIS","grp":[],"codes":[{"key":"services","value":["130","128"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (66, 'PRD', 'TVP', TO_DATE('26/02/2020 13:17:11', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 11:03:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"66","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRD","icon":"euro","cia":"TVP","codes":[{"key":"groups","value":["107","106"]},{"key":"services","value":["128"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CIA, MLM_CREATED, MLM_UPDATED, 
    MLM_JSON)
 Values
   (68, 'PRD', 'IBT', TO_DATE('26/02/2020 13:20:02', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:44', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"68","name":[{"key":"PT","value":"Venda"},{"key":"EN","value":"Sales"},{"key":"ES","value":"Venta"}],"act":true,"del":false,"env":"PRD","icon":"euro","cia":"IBT","srv":[{"id":"129","name":[{"key":"PT","value":"Touring Club"},{"key":"EN","value":"Touring Club"},{"key":"ES","value":"Touring Club"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.touringclub.es","params":[],"readonly":false},"ext":true,"fie":false},{"id":"128","name":[{"key":"PT","value":"Call Center"},{"key":"EN","value":"Call Center"},{"key":"ES","value":"Call Center"}],"act":true,"del":false,"env":"PRD","icon":"headset_mic","url":{"url":"https://newtour.travelplan.es","params":[],"readonly":false},"ext":true,"fie":false}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_ENV, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (50, 'PRD', TO_DATE('26/02/2020 8:55:31', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:58:23', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"50","name":[{"key":"PT","value":"Receptivo"},{"key":"EN","value":"Receptive"},{"key":"ES","value":"Receptivo"}],"act":true,"del":false,"env":"PRD","icon":"business","grp":[],"codes":[{"key":"services","value":["117"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_CIA, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (175, 'WIS', TO_DATE('27/03/2020 12:01:32', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 10:58:33', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"175","name":[{"key":"PT","value":"Gestão comercial"},{"key":"EN","value":"Commercial management"},{"key":"ES","value":"Gestión Comercial"}],"act":true,"del":false,"icon":"picture_in_picture","cia":"WIS","srv":[],"codes":[{"key":"groups","value":["179","176","175"]}]}');
Insert into NWTMNU.MNU_LOGIN_MENU
   (MLM_ID, MLM_CREATED, MLM_UPDATED, MLM_JSON)
 Values
   (177, TO_DATE('10/06/2020 9:52:13', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('05/08/2020 8:37:54', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"177","name":[{"key":"PT","value":"tester2"},{"key":"EN","value":"tester2"},{"key":"ES","value":"tester2"}],"act":true,"del":true}');
COMMIT;
