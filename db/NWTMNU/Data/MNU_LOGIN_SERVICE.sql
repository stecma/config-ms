SET DEFINE OFF;
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (55, '/api/v1/user', TO_DATE('10/01/2020 20:53:47', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('06/08/2020 14:00:46', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"55","name":[{"key":"PT","value":"Utilizadores"},{"key":"EN","value":"Users"},{"key":"ES","value":"Usuarios"}],"act":true,"del":false,"icon":"face","url":{"url":"/api/v1/user","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (96, 'PRE', 'http://formsweb-pre.globalia.com:9001/forms/frmservlet??config=nwt_emisor', TO_DATE('25/02/2020 14:42:59', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"96","name":[{"key":"PT","value":"Newtour"},{"key":"EN","value":"Newtour"},{"key":"ES","value":"Newtour"}],"act":true,"del":false,"env":"PRE","icon":"flight_takeoff","url":{"url":"http://formsweb-pre.globalia.com:9001/forms/frmservlet?","params":[{"name":"config","value":"nwt_emisor"}],"readonly":false},"ext":true,"fie":true}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (105, 'DEV', 'http://test.operec.travelplan.es', TO_DATE('25/02/2020 17:54:12', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"105","name":[{"key":"PT","value":"Operec"},{"key":"EN","value":"Operec"},{"key":"ES","value":"Operec"}],"act":true,"del":false,"env":"DEV","icon":"add","url":{"url":"http://test.operec.travelplan.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (120, 'PRE', 'http://b2cpre.travelplan.es/b2c/jsp/principal.jsp??tipoBusqueda=CIRSINVUE', TO_DATE('26/02/2020 12:43:00', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"120","name":[{"key":"PT","value":"Circuitos Internacionais"},{"key":"EN","value":"International Circuits"},{"key":"ES","value":"Circuitos Internacional"}],"act":true,"del":false,"env":"PRE","icon":"public","url":{"url":"http://b2cpre.travelplan.es/b2c/jsp/principal.jsp?","params":[{"name":"tipoBusqueda","value":"CIRSINVUE"}],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (122, 'PRE', 'http://b2cpre.travelplan.es/b2c/jsp/principal.jsp?', TO_DATE('26/02/2020 12:48:34', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"122","name":[{"key":"PT","value":"F�rias"},{"key":"EN","value":"Holidays"},{"key":"ES","value":"Vacacional"}],"act":true,"del":false,"env":"PRE","icon":"public","url":{"url":"http://b2cpre.travelplan.es/b2c/jsp/principal.jsp?","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (124, 'PRE', 'http://pre.welcomebeds.com', TO_DATE('26/02/2020 13:04:26', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"124","name":[{"key":"PT","value":"WelcomeBeds"},{"key":"EN","value":"WelcomeBeds"},{"key":"ES","value":"WelcomeBeds"}],"act":true,"del":false,"env":"PRE","icon":"public","url":{"url":"http://pre.welcomebeds.com","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (98, 'PRE', 'http://pre.xtg.welcomebeds.com/xtg-csvgenerator/?', TO_DATE('25/02/2020 14:48:05', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"98","name":[{"key":"PT","value":"Relat�rios WelcomeBeds"},{"key":"EN","value":"WelcomeBeds reports"},{"key":"ES","value":"Informes WelcomeBeds"}],"act":true,"del":false,"env":"PRE","icon":"filter","url":{"url":"http://pre.xtg.welcomebeds.com/xtg-csvgenerator/?","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (123, 'PRE', 'http://prenwtweb.travelplan.es/', TO_DATE('26/02/2020 12:59:39', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"123","name":[{"key":"PT","value":"Call Center"},{"key":"EN","value":"Call Center"},{"key":"ES","value":"Call Center"}],"act":true,"del":false,"env":"PRE","icon":"headset_mic","url":{"url":"http://prenwtweb.travelplan.es/","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (126, 'PRD', 'http://b2c.travelplan.es/b2c/jsp/principal.jsp??tipoBusqueda=CIRCUITO', TO_DATE('26/02/2020 13:11:01', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:43', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"126","name":[{"key":"PT","value":"Circuito"},{"key":"EN","value":"Circuit"},{"key":"ES","value":"Circuito"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://b2c.travelplan.es/b2c/jsp/principal.jsp?","params":[{"name":"tipoBusqueda","value":"CIRCUITO"}],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (127, 'PRD', 'http://b2c.travelplan.es/b2c/jsp/principal.jsp?', TO_DATE('26/02/2020 13:12:00', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"127","name":[{"key":"PT","value":"F�rias"},{"key":"EN","value":"Holidays"},{"key":"ES","value":"Vacacional"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://b2c.travelplan.es/b2c/jsp/principal.jsp?","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (130, 'PRD', 'https://www.welcomebeds.com', TO_DATE('26/02/2020 13:21:04', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"130","name":[{"key":"PT","value":"WelcomeBeds"},{"key":"EN","value":"WelcomeBeds"},{"key":"ES","value":"WelcomeBeds"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"https://www.welcomebeds.com","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (131, 'PRD', 'http://www.travelplan.es', TO_DATE('26/02/2020 13:22:47', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"131","name":[{"key":"PT","value":"Travelplan"},{"key":"EN","value":"Travelplan"},{"key":"ES","value":"Travelplan"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.travelplan.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (132, 'PRD', 'http://www.travelplan.pt', TO_DATE('26/02/2020 13:23:24', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:43', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"132","name":[{"key":"PT","value":"Travelplan PT"},{"key":"EN","value":"Travelplan PT"},{"key":"ES","value":"Travelplan PT"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.travelplan.pt","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (135, 'PRD', 'http://www.travelplan.us', TO_DATE('26/02/2020 13:25:41', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"135","name":[{"key":"PT","value":"Travelplan US"},{"key":"EN","value":"Travelplan US"},{"key":"ES","value":"Travelplan US"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.travelplan.us","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (138, 'PRD', 'http://www.iberrail.es', TO_DATE('26/02/2020 13:27:41', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"138","name":[{"key":"PT","value":"Iberrail"},{"key":"EN","value":"Iberrail"},{"key":"ES","value":"Iberrail"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.iberrail.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (60, '/api/v1/master', TO_DATE('10/01/2020 21:18:22', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"60","name":[{"key":"ES","value":"Maestros"},{"key":"PT","value":"Mestres"},{"key":"EN","value":"Masters"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (63, 'DEV', 'http://test.newtourweb.travelplan.es/newtour/jsp/principal.jsp', TO_DATE('10/01/2020 21:30:04', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"63","name":[{"key":"EN","value":"NewTour Operations"},{"key":"ES","value":"Operativa NewTour"},{"key":"PT","value":"Opera��es NewTour"}],"act":true,"del":false,"env":"DEV","icon":"touch_app","url":{"url":"http://test.newtourweb.travelplan.es/newtour/jsp/principal.jsp","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (64, 'DEV', 'http://test.revenue.welcomebeds.com/session/login', TO_DATE('10/01/2020 21:31:01', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"64","name":[{"key":"PT","value":"HOPE"},{"key":"EN","value":"HOPE"},{"key":"ES","value":"HOPE"}],"act":true,"del":false,"env":"DEV","icon":"group_work","url":{"url":"http://test.revenue.welcomebeds.com/session/login","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (95, 'PRD', 'http://informesnwt.globalia.com/informes/index.php', TO_DATE('25/02/2020 13:09:03', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"95","name":[{"key":"PT","value":"Iniciador de relat�rios"},{"key":"EN","value":"Report Launcher"},{"key":"ES","value":"Lanzador de Informes"}],"act":true,"del":false,"env":"PRD","icon":"visibility","url":{"url":"http://informesnwt.globalia.com/informes/index.php","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (100, 'PRD', 'http://xtg.welcomebeds.com/xtg-csvgenerator/?', TO_DATE('25/02/2020 14:56:51', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"100","name":[{"key":"PT","value":"Relat�rios WelcomeBeds"},{"key":"EN","value":"WelcomeBeds reports"},{"key":"ES","value":"Informes WelcomeBeds"}],"act":true,"del":false,"env":"PRD","icon":"filter","url":{"url":"http://xtg.welcomebeds.com/xtg-csvgenerator/?","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (112, 'PRE', 'http://prenwt.travelplan.es/newtour/jsp/principal.jsp ', TO_DATE('26/02/2020 8:36:11', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"112","name":[{"key":"PT","value":"Opera��es NewTour"},{"key":"EN","value":"NewTour Operations"},{"key":"ES","value":"Operativa NewTour"}],"act":true,"del":false,"env":"PRE","icon":"touch_app","url":{"url":"http://prenwt.travelplan.es/newtour/jsp/principal.jsp ","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (113, 'PRE', 'http://prenwt.travelplan.es', TO_DATE('26/02/2020 8:38:52', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"113","name":[{"key":"PT","value":"Site operacional"},{"key":"EN","value":"Operational website"},{"key":"ES","value":"Operativa Web"}],"act":true,"del":false,"env":"PRE","icon":"language","url":{"url":"http://prenwt.travelplan.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (116, 'PRD', 'http://operec.travelplan.es', TO_DATE('26/02/2020 8:49:02', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"116","name":[{"key":"PT","value":"Operec"},{"key":"EN","value":"Operec"},{"key":"ES","value":"Operec"}],"act":true,"del":false,"env":"PRD","icon":"track_changes","url":{"url":"http://operec.travelplan.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (177, '/api/v1/master/credential/brand', TO_DATE('16/04/2020 9:12:12', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:39', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"177","name":[{"key":"PT","value":"Marcas comerciais"},{"key":"EN","value":"Trademarks"},{"key":"ES","value":"Marcas Comerciales"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/brand","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (201, '/api/v1/master/credential/top_destination', TO_DATE('19/05/2020 12:08:55', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"201","name":[{"key":"PT","value":"Principais destinos"},{"key":"EN","value":"Top destinations"},{"key":"ES","value":"Top destinos"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/top_destination","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (182, '/api/v1/master/credential/distribution_model', TO_DATE('16/04/2020 15:54:12', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"182","name":[{"key":"PT","value":"Modelos de distribui��o"},{"key":"EN","value":"Distribution models"},{"key":"ES","value":"Modelos de distribuci�n"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/distribution_model","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (186, '/api/v1/master/credential/service_type', TO_DATE('17/04/2020 9:27:47', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"186","name":[{"key":"PT","value":"Tipos de servi�o"},{"key":"EN","value":"Service types"},{"key":"ES","value":"Tipos de servicio"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/service_type","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (188, '/api/v1/master/credential/product_origin', TO_DATE('17/04/2020 9:33:39', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"188","name":[{"key":"PT","value":"Origens do produto"},{"key":"EN","value":"Product origin"},{"key":"ES","value":"Origenes del producto"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/product_origin","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (192, '/api/v1/master/credential/currency', TO_DATE('27/04/2020 11:13:01', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"192","name":[{"key":"PT","value":"Moedas"},{"key":"EN","value":"Currencies"},{"key":"ES","value":"Divisas"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/currency","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (194, '/api/v1/master/credential/buy_model', TO_DATE('27/04/2020 11:20:49', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:43', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"194","name":[{"key":"PT","value":"Modelos de compra"},{"key":"EN","value":"Buy models"},{"key":"ES","value":"Modelos de compra"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/buy_model","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (204, '/test', TO_DATE('10/06/2020 9:50:08', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('05/08/2020 8:13:20', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"204","name":[{"key":"PT","value":"tester2"},{"key":"EN","value":"tester2"},{"key":"ES","value":"tester2"}],"act":true,"del":true,"url":{"url":"/test","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (205, '/api/v1/master/credential/client_type', TO_DATE('15/06/2020 7:42:50', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"205","name":[{"key":"PT","value":"Tipos de cliente"},{"key":"EN","value":"Customer types"},{"key":"ES","value":"Tipos de cliente"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/client_type","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (125, 'PRD', 'http://b2c.travelplan.es/b2c/jsp/principal.jsp??tipoBusqueda=CIRSINVUE', TO_DATE('26/02/2020 13:10:07', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"125","name":[{"key":"PT","value":"Circuito internacional"},{"key":"EN","value":"International circuit"},{"key":"ES","value":"Circuito internacional"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://b2c.travelplan.es/b2c/jsp/principal.jsp?","params":[{"name":"tipoBusqueda","value":"CIRSINVUE"}],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (117, 'PRD', 'http://forms.newtour.es:9001/forms/frmservlet??config=nwt_receptivo', TO_DATE('26/02/2020 8:54:31', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"117","name":[{"key":"PT","value":"Receptor de Newtour"},{"key":"EN","value":"Newtour destination"},{"key":"ES","value":"Newtour Receptivo"}],"act":true,"del":false,"env":"PRD","icon":"business","url":{"url":"http://forms.newtour.es:9001/forms/frmservlet?","params":[{"name":"config","value":"nwt_receptivo"}],"readonly":false},"ext":true,"fie":true}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (118, 'DEV', 'http://testnwtweb.travelplan.es/', TO_DATE('26/02/2020 11:14:07', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"118","name":[{"key":"PT","value":"Call Center"},{"key":"EN","value":"Call Center"},{"key":"ES","value":"Call Center"}],"act":true,"del":false,"env":"DEV","icon":"headset_mic","url":{"url":"http://testnwtweb.travelplan.es/","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (119, 'DEV', 'http://test.welcomebeds.com', TO_DATE('26/02/2020 11:17:28', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"119","name":[{"key":"PT","value":"WelcomeBeds"},{"key":"EN","value":"WelcomeBeds"},{"key":"ES","value":"WelcomeBeds"}],"act":true,"del":false,"env":"DEV","icon":"public","url":{"url":"http://test.welcomebeds.com","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (115, 'PRD', 'http://newtourweb.travelplan.es/jsp/principal.jsp', TO_DATE('26/02/2020 8:47:10', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"115","name":[{"key":"PT","value":"Opera��es NewTour"},{"key":"EN","value":"NewTour Operations"},{"key":"ES","value":"Operativa NewTour"}],"act":true,"del":false,"env":"PRD","icon":"touch_app","url":{"url":"http://newtourweb.travelplan.es/jsp/principal.jsp","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (129, 'PRD', 'http://www.touringclub.es', TO_DATE('26/02/2020 13:19:24', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"129","name":[{"key":"PT","value":"Touring Club"},{"key":"EN","value":"Touring Club"},{"key":"ES","value":"Touring Club"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.touringclub.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (137, 'PRD', 'http://www.travelplannet.es', TO_DATE('26/02/2020 13:27:16', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"137","name":[{"key":"PT","value":"Travelplannet"},{"key":"EN","value":"Travelplannet"},{"key":"ES","value":"Travelplannet"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.travelplannet.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (58, '/api/v1/group', TO_DATE('10/01/2020 21:13:00', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"58","name":[{"key":"ES","value":"Grupo de servicios"},{"key":"PT","value":"Grupo de Servi�o"},{"key":"EN","value":"Service Group"}],"act":true,"del":false,"icon":"view_list","url":{"url":"/api/v1/group","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (175, '/api/v1/credential', TO_DATE('27/03/2020 11:59:25', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('21/10/2020 13:05:09', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"175","name":[{"key":"PT","value":"Gerenciamento de credenciais"},{"key":"EN","value":"Credentials management"},{"key":"ES","value":"Gesti�n credenciales"}],"act":true,"del":false,"icon":"credit_card","url":{"url":"/api/v1/credential","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (178, '/api/v1/master/credential/country', TO_DATE('16/04/2020 13:38:00', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"178","name":[{"key":"PT","value":"Pa�ses"},{"key":"EN","value":"Countries"},{"key":"ES","value":"Paises"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/country","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (185, '/api/v1/master/credential/platform', TO_DATE('17/04/2020 9:26:59', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"185","name":[{"key":"PT","value":"Plataformas"},{"key":"EN","value":"Platforms"},{"key":"ES","value":"Plataformas"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/platform","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (208, 'DEV', 'https://pregrupos.welcomebeds.com/grupos/', TO_DATE('18/09/2020 9:34:55', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('21/09/2020 10:42:27', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"208","name":[{"key":"EN","value":"Groups"},{"key":"PT","value":"Grupos"},{"key":"ES","value":"Grupos"}],"act":true,"del":false,"env":"DEV","icon":"group_work","url":{"url":"https://pregrupos.welcomebeds.com/grupos/","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (190, '/api/v1/master/credential/environments', TO_DATE('17/04/2020 9:37:43', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"190","name":[{"key":"PT","value":"Tipos de ambiente"},{"key":"EN","value":"Environment Types"},{"key":"ES","value":"Tipos de entorno"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/environments","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (184, '/api/v1/master/credential/sales_model', TO_DATE('17/04/2020 9:19:19', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"184","name":[{"key":"PT","value":"Modelos de vendas"},{"key":"EN","value":"Sale models"},{"key":"ES","value":"Modelos de venta"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/sales_model","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (187, '/api/v1/master/credential/rate_type', TO_DATE('17/04/2020 9:30:00', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:43', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"187","name":[{"key":"PT","value":"Tipos de taxa"},{"key":"EN","value":"Rate types"},{"key":"ES","value":"Tipos de tarifa"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/rate_type","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (198, '/api/v1/master/credential/external_system', TO_DATE('11/05/2020 15:44:58', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"198","name":[{"key":"PT","value":"Sistemas externos"},{"key":"EN","value":"External systems"},{"key":"ES","value":"Sistemas externos"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/external_system","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (200, '/api/v1/supplier', TO_DATE('13/05/2020 11:47:38', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"200","name":[{"key":"PT","value":"Criar provedor PULL"},{"key":"EN","value":"Create supplier PULL"},{"key":"ES","value":"Crear proveedor PULL"}],"act":true,"del":false,"icon":"reorder","url":{"url":"/api/v1/supplier","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (207, 'test', TO_DATE('05/08/2020 8:13:04', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('05/08/2020 8:13:27', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"207","name":[{"key":"EN","value":"test"},{"key":"ES","value":"test"},{"key":"PT","value":"test"}],"act":false,"del":true,"url":{"url":"test","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (102, 'PRD', 'http://www.welcomebeds.com', TO_DATE('25/02/2020 15:48:01', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('26/02/2020 20:22:32', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":102,"env":"PRD","name":[{"key":"PT","value":"Supplier''s website"},{"key":"EN","value":"Supplier''s website"},{"key":"ES","value":"Supplier''s website"}],"act":true,"icon":"language","url":{"url":"https://www.welcomebeds.com","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (106, 'DEV', 'http://testnwtweb.travelplan.es:8180/newtour/jsp/principal.jsp', TO_DATE('25/02/2020 17:56:39', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"106","name":[{"key":"PT","value":"Site operacional"},{"key":"EN","value":"Operational website"},{"key":"ES","value":"Operativa Web"}],"act":true,"del":false,"env":"DEV","icon":"language","url":{"url":"http://testnwtweb.travelplan.es:8180/newtour/jsp/principal.jsp","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (121, 'PRE', 'http://b2cpre.travelplan.es/b2c/jsp/principal.jsp??tipoBusqueda=CIRCUITO', TO_DATE('26/02/2020 12:45:17', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"121","name":[{"key":"PT","value":"Circuito"},{"key":"EN","value":"Circuit"},{"key":"ES","value":"Circuito"}],"act":true,"del":false,"env":"PRE","icon":"public","url":{"url":"http://b2cpre.travelplan.es/b2c/jsp/principal.jsp?","params":[{"name":"tipoBusqueda","value":"CIRCUITO"}],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (103, 'PRD', 'http://informesnwt.globalia.com/compilar/index.php', TO_DATE('25/02/2020 15:53:55', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"103","name":[{"key":"PT","value":"Compilador"},{"key":"EN","value":"Compiler"},{"key":"ES","value":"Compilador"}],"act":true,"del":false,"env":"PRD","icon":"report","url":{"url":"http://informesnwt.globalia.com/compilar/index.php","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (101, 'PRD', 'http://revenue.welcomebeds.com/session/login', TO_DATE('25/02/2020 15:43:13', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"101","name":[{"key":"PT","value":"HOPE"},{"key":"EN","value":"HOPE"},{"key":"ES","value":"HOPE"}],"act":true,"del":false,"env":"PRD","icon":"group_work","url":{"url":"http://revenue.welcomebeds.com/session/login","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (128, 'PRD', 'https://newtour.travelplan.es', TO_DATE('26/02/2020 13:13:37', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"128","name":[{"key":"PT","value":"Call Center"},{"key":"EN","value":"Call Center"},{"key":"ES","value":"Call Center"}],"act":true,"del":false,"env":"PRD","icon":"headset_mic","url":{"url":"https://newtour.travelplan.es","params":[],"readonly":false},"ext":true,"fie":true}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (134, 'PRD', 'http://www.travelplaninternacional.com/jsp/inicio/home_br.jsp', TO_DATE('26/02/2020 13:24:48', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"134","name":[{"key":"PT","value":"Travelplan BR"},{"key":"EN","value":"Travelplan BR"},{"key":"ES","value":"Travelplan BR"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.travelplaninternacional.com/jsp/inicio/home_br.jsp","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (136, 'PRD', 'http://www.travelplaninternacional.com/jsp/inicio/home_eu.jsp', TO_DATE('26/02/2020 13:26:41', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"136","name":[{"key":"PT","value":"Travelplan EU"},{"key":"EN","value":"Travelplan EU"},{"key":"ES","value":"Travelplan EU"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.travelplaninternacional.com/jsp/inicio/home_eu.jsp","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (62, 'DEV', 'http://formsweb-des.globalia.com:9001/forms/frmservlet??config=nwt_emisor', TO_DATE('10/01/2020 21:24:58', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"62","name":[{"key":"EN","value":"NewTour"},{"key":"PT","value":"NewTour"},{"key":"ES","value":"NewTour"}],"act":true,"del":false,"env":"DEV","icon":"flight_takeoff","url":{"url":"http://formsweb-des.globalia.com:9001/forms/frmservlet?","params":[{"name":"config","value":"nwt_emisor"}],"readonly":false},"ext":true,"fie":true}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (97, 'PRE', 'http://pre.revenue.welcomebeds.com/session/login', TO_DATE('25/02/2020 14:45:36', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"97","name":[{"key":"PT","value":"HOPE"},{"key":"EN","value":"HOPE"},{"key":"ES","value":"HOPE"}],"act":true,"del":false,"env":"PRE","icon":"group_work","url":{"url":"http://pre.revenue.welcomebeds.com/session/login","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (99, 'PRD', 'http://forms.newtour.es:9001/forms/frmservlet??config=nwt_emisor', TO_DATE('25/02/2020 14:54:30', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"99","name":[{"key":"PT","value":"Newtour"},{"key":"EN","value":"Newtour"},{"key":"ES","value":"Newtour"}],"act":true,"del":false,"env":"PRD","icon":"flight_takeoff","url":{"url":"http://forms.newtour.es:9001/forms/frmservlet?","params":[{"name":"config","value":"nwt_emisor"}],"readonly":false},"ext":true,"fie":true}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (111, 'PRE', 'http://formsweb-pre.globalia.com:9001/forms/frmservlet??config=nwt_receptivo', TO_DATE('26/02/2020 8:29:23', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"111","name":[{"key":"PT","value":"Receptor de Newtour"},{"key":"EN","value":"Newtour destination"},{"key":"ES","value":"Newtour Receptivo"}],"act":true,"del":false,"env":"PRE","icon":"business","url":{"url":"http://formsweb-pre.globalia.com:9001/forms/frmservlet?","params":[{"name":"config","value":"nwt_receptivo"}],"readonly":false},"ext":true,"fie":true}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (114, 'PRE', 'http://pre.operec.travelplan.es', TO_DATE('26/02/2020 8:40:05', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"114","name":[{"key":"PT","value":"Operec"},{"key":"EN","value":"Operec"},{"key":"ES","value":"Operec"}],"act":true,"del":false,"env":"PRE","icon":"add","url":{"url":"http://pre.operec.travelplan.es","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (183, '/api/v1/master/credential/sales_channel', TO_DATE('16/04/2020 15:56:46', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"183","name":[{"key":"PT","value":"Canais de vendas"},{"key":"EN","value":"Sales channels"},{"key":"ES","value":"Canales de venta"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/sales_channel","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (189, '/api/v1/master/credential/request', TO_DATE('17/04/2020 9:35:37', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"189","name":[{"key":"PT","value":"Tipos de solicita��o"},{"key":"EN","value":"Request types"},{"key":"ES","value":"Tipos de petici�n"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/request","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (193, '/api/v1/master/credential/client_pref', TO_DATE('27/04/2020 11:19:52', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"193","name":[{"key":"PT","value":"Prefer�ncias do cliente"},{"key":"EN","value":"Client preferences"},{"key":"ES","value":"Preferencias del cliente"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/client_pref","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (202, '/api/v1/master/credential/billing_type', TO_DATE('19/05/2020 14:25:04', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"202","name":[{"key":"PT","value":"Tipos de cobran�a"},{"key":"EN","value":"Invoicing types"},{"key":"ES","value":"Tipos de facturaci�n"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/billing_type","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (209, '/api/v1/match/supplier', TO_DATE('21/09/2020 11:05:23', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('21/09/2020 11:05:57', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"209","name":[{"key":"EN","value":"Supplier Match"},{"key":"ES","value":"Match proveedores PULL"},{"key":"PT","value":"Match proveedores PULL"}],"act":true,"del":false,"icon":"close_fullscreen","url":{"url":"/api/v1/match/supplier","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (110, 'DEV', 'http://formsweb-des.globalia.com:9001/forms/frmservlet??config=nwt_receptivo', TO_DATE('25/02/2020 20:56:55', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"110","name":[{"key":"PT","value":"Receptor de Newtour"},{"key":"EN","value":"Newtour destination"},{"key":"ES","value":"Newtour Receptivo"}],"act":true,"del":false,"env":"DEV","icon":"business","url":{"url":"http://formsweb-des.globalia.com:9001/forms/frmservlet?","params":[{"name":"config","value":"nwt_receptivo"}],"readonly":false},"ext":true,"fie":true}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_ENV, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, 
    MLS_JSON)
 Values
   (133, 'PRD', 'http://www.travelplaninternacional.com/jsp/inicio/home_ar.jsp', TO_DATE('26/02/2020 13:24:13', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:40', 'DD/MM/YYYY HH24:MI:SS'), 
    '{"id":"133","name":[{"key":"PT","value":"Travelplan AR"},{"key":"EN","value":"Travelplan AR"},{"key":"ES","value":"Travelplan AR"}],"act":true,"del":false,"env":"PRD","icon":"public","url":{"url":"http://www.travelplaninternacional.com/jsp/inicio/home_ar.jsp","params":[],"readonly":false},"ext":true,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (56, '/api/v1/menu', TO_DATE('10/01/2020 20:57:51', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"56","name":[{"key":"PT","value":"Menu"},{"key":"ES","value":"Menu"},{"key":"EN","value":"Menu"}],"act":true,"del":false,"icon":"reorder","url":{"url":"/api/v1/menu","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (59, '/api/v1/service', TO_DATE('10/01/2020 21:16:33', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"59","name":[{"key":"ES","value":"Servicios"},{"key":"EN","value":"Services"},{"key":"PT","value":"Servi�os"}],"act":true,"del":false,"icon":"extension","url":{"url":"/api/v1/service","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (176, '/api/v1/master/credential/languages', TO_DATE('14/04/2020 18:36:56', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"176","name":[{"key":"PT","value":"L�nguas"},{"key":"EN","value":"Languages"},{"key":"ES","value":"Idiomas"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/languages","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (197, '/api/v1/master/credential/service_distribution', TO_DATE('04/05/2020 11:19:44', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:42', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"197","name":[{"key":"PT","value":"Distribui��o de servi�os"},{"key":"EN","value":"Service distribution"},{"key":"ES","value":"V�as de distribuci�n"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/service_distribution","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (203, '/api/v1/supplier/filter', TO_DATE('22/05/2020 11:06:31', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('14/08/2020 14:13:12', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"203","name":[{"key":"PT","value":"Gest�o de provedores PULL"},{"key":"EN","value":"Suppliers PULL management"},{"key":"ES","value":"Gesti�n de proveedores PULL"}],"act":true,"del":false,"icon":"reorder","url":{"url":"/api/v1/supplier/filter","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (191, '/api/v1/master/credential/depth', TO_DATE('17/04/2020 9:38:36', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"191","name":[{"key":"PT","value":"Departamentos"},{"key":"EN","value":"Departments"},{"key":"ES","value":"Departamentos"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/depth","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (199, '/api/v1/master/credential/users', TO_DATE('11/05/2020 15:45:35', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:41', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"199","name":[{"key":"PT","value":"Utilizadores"},{"key":"EN","value":"Users"},{"key":"ES","value":"Usuarios"}],"act":true,"del":false,"icon":"bookmark","url":{"url":"/api/v1/master/credential/users","params":[],"readonly":false},"ext":false,"fie":false}');
Insert into NWTMNU.MNU_LOGIN_SERVICE
   (MLS_ID, MLS_SERVICE, MLS_CREATED, MLS_UPDATED, MLS_JSON)
 Values
   (210, '/api/v1/match/customer', TO_DATE('28/09/2020 8:10:14', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('21/10/2020 13:06:21', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"210","name":[{"key":"PT","value":"Match clientes"},{"key":"EN","value":"Customer match"},{"key":"ES","value":"Match clientes"}],"act":true,"del":false,"icon":"close_fullscreen","url":{"url":"/api/v1/match/customer","params":[],"readonly":false},"ext":false,"fie":false}');
COMMIT;
