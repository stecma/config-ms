SET DEFINE OFF;
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (45, TO_DATE('13/01/2020 13:03:14', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 11:15:43', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"45","name":[{"key":"ES","value":"Gesti�n de men�s"},{"key":"PT","value":"Gerenciamento de menu"},{"key":"EN","value":"Menu management"}],"act":true,"del":false,"icon":"storage","codes":[{"key":"services","value":["59","58","56"]}]}');
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_ENV, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (105, 'PRE', TO_DATE('26/02/2020 12:50:00', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('30/10/2020 11:11:39', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"105","name":[{"key":"PT","value":"B2C"},{"key":"EN","value":"B2C"},{"key":"ES","value":"B2C"}],"act":true,"del":false,"env":"PRE","icon":"public","codes":[{"key":"services","value":["122","121","120"]}]}');
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_ENV, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (106, 'PRD', TO_DATE('26/02/2020 13:12:32', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:45', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"106","name":[{"key":"PT","value":"B2C"},{"key":"EN","value":"B2C"},{"key":"ES","value":"B2C"}],"act":true,"del":false,"env":"PRD","icon":"public","codes":[{"key":"services","value":["127","126","125"]}]}');
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_ENV, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (107, 'PRD', TO_DATE('26/02/2020 13:28:57', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('16/07/2020 8:42:45', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"107","name":[{"key":"PT","value":"B2B"},{"key":"EN","value":"B2B"},{"key":"ES","value":"B2B"}],"act":true,"del":false,"env":"PRD","icon":"public","codes":[{"key":"services","value":["131","132","135","133","134","136","137","138"]}]}');
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (179, TO_DATE('22/05/2020 11:12:12', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('21/09/2020 11:06:58', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"179","name":[{"key":"PT","value":"Provedores Pull"},{"key":"EN","value":"Suppliers Pull"},{"key":"ES","value":"Proveedores Pull"}],"act":true,"del":false,"icon":"storage","codes":[{"key":"services","value":["203","200","209"]}]}');
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (175, TO_DATE('27/03/2020 12:00:38', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('21/10/2020 13:07:39', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"175","name":[{"key":"PT","value":"Credenciais do cliente"},{"key":"EN","value":"Customer credentials"},{"key":"ES","value":"Credenciales clientes"}],"act":true,"del":false,"icon":"horizontal_split","codes":[{"key":"services","value":["175","210"]}]}');
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (176, TO_DATE('14/04/2020 18:38:37', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('05/08/2020 8:17:51', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"176","name":[{"key":"PT","value":"Mestres"},{"key":"EN","value":"Masters"},{"key":"ES","value":"Maestros"}],"act":true,"del":false,"icon":"book","codes":[{"key":"services","value":["176","177","193","192","194","178","202","201","183","182","186","184","189","205","187","188","185","190","191","197","198","199"]}]}');
Insert into NWTMNU.MNU_LOGIN_GROUP
   (MLG_ID, MLG_CREATED, MLG_UPDATED, MLG_JSON)
 Values
   (180, TO_DATE('10/06/2020 9:50:40', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('05/08/2020 8:18:26', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"180","name":[{"key":"PT","value":"tester2"},{"key":"EN","value":"tester2"},{"key":"ES","value":"tester2"}],"act":true,"del":true}');
COMMIT;
