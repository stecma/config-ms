DROP TABLE GESCOM.ENTORNOS_CREDENCIALES CASCADE CONSTRAINTS;

CREATE TABLE GESCOM.ENTORNOS_CREDENCIALES
(
  ENT_CODENT  VARCHAR2(6 BYTE)                  NOT NULL,
  ENT_NOMENT  VARCHAR2(50 BYTE)                 NOT NULL,
  ENT_JSON    VARCHAR2(2000 BYTE)               NOT NULL
)
TABLESPACE NEWTOUR_DAT02
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE;

COMMENT ON TABLE GESCOM.ENTORNOS_CREDENCIALES IS 'Tabla auxiliar para almacenar los entornos de las credenciales';

COMMENT ON COLUMN GESCOM.ENTORNOS_CREDENCIALES.ENT_CODENT IS 'Identificador del entorno de las credenciales';

COMMENT ON COLUMN GESCOM.ENTORNOS_CREDENCIALES.ENT_NOMENT IS 'Nombre del entorno de las credenciales';


CREATE UNIQUE INDEX GESCOM.ENT_PK_I ON GESCOM.ENTORNOS_CREDENCIALES
(ENT_CODENT)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE GESCOM.ENTORNOS_CREDENCIALES ADD (
  CONSTRAINT ENT_PK
  PRIMARY KEY
  (ENT_CODENT)
  USING INDEX GESCOM.ENT_PK_I
  ENABLE VALIDATE);


CREATE OR REPLACE PUBLIC SYNONYM ENTORNOS_CREDENCIALES FOR GESCOM.ENTORNOS_CREDENCIALES;


GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON GESCOM.ENTORNOS_CREDENCIALES TO PUBLIC;
