DROP TABLE GESCOM.TIPOS_TARIFA_CREDENCIALES CASCADE CONSTRAINTS;

CREATE TABLE GESCOM.TIPOS_TARIFA_CREDENCIALES
(
  TTA_CODTTA  VARCHAR2(6 BYTE)                  NOT NULL,
  TTA_NOMTTA  VARCHAR2(50 BYTE)                 NOT NULL,
  TTA_JSON    VARCHAR2(2000 BYTE)               NOT NULL
)
TABLESPACE NEWTOUR_DAT02
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE;

COMMENT ON TABLE GESCOM.TIPOS_TARIFA_CREDENCIALES IS 'Tabla auxiliar para almacenar los tipos de tarifa de las credenciales';

COMMENT ON COLUMN GESCOM.TIPOS_TARIFA_CREDENCIALES.TTA_CODTTA IS 'Identificador del tipo de tarifa de las credenciales';

COMMENT ON COLUMN GESCOM.TIPOS_TARIFA_CREDENCIALES.TTA_NOMTTA IS 'Nombre del tipo de tarifa de las credenciales';


CREATE UNIQUE INDEX GESCOM.TTA_PK_I ON GESCOM.TIPOS_TARIFA_CREDENCIALES
(TTA_CODTTA)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE GESCOM.TIPOS_TARIFA_CREDENCIALES ADD (
  CONSTRAINT TTA_PK
  PRIMARY KEY
  (TTA_CODTTA)
  USING INDEX GESCOM.TTA_PK_I
  ENABLE VALIDATE);


CREATE OR REPLACE PUBLIC SYNONYM TIPOS_TARIFA_CREDENCIALES FOR GESCOM.TIPOS_TARIFA_CREDENCIALES;


GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON GESCOM.TIPOS_TARIFA_CREDENCIALES TO PUBLIC;
