DROP TABLE GESCOM.PROVEEDOR_CREDENCIALES CASCADE CONSTRAINTS;

CREATE TABLE GESCOM.PROVEEDOR_CREDENCIALES
(
  PRC_CODPRC   VARCHAR2(40 BYTE)                NOT NULL,
  PRC_CODTSE   VARCHAR2(6 BYTE)                 NOT NULL,
  PRC_CODSIS   VARCHAR2(15 BYTE)                NOT NULL,
  PRC_CODMCE   VARCHAR2(6 BYTE)                 NOT NULL,
  PRC_CODMLU   VARCHAR2(50 BYTE)                NOT NULL,
  PRC_CODCPC   VARCHAR2(6 BYTE)                 NOT NULL,
  PRC_JSON     CLOB                             NOT NULL,
  PRC_CREATED  DATE                             DEFAULT SYSDATE               NOT NULL,
  PRC_UPDATED  DATE                             DEFAULT SYSDATE               NOT NULL
)
LOB (PRC_JSON) STORE AS BASICFILE (
  TABLESPACE  NEWTOUR_DAT02
  ENABLE      STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING
  STORAGE    (
              INITIAL          64K
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
              BUFFER_POOL      DEFAULT
             ))
TABLESPACE NEWTOUR_DAT02
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE;

COMMENT ON TABLE GESCOM.PROVEEDOR_CREDENCIALES IS 'Tabla para almacenar los proveedores de las credenciales';

COMMENT ON COLUMN GESCOM.PROVEEDOR_CREDENCIALES.PRC_CODPRC IS 'Identificador de la credencial del proveedor';

COMMENT ON COLUMN GESCOM.PROVEEDOR_CREDENCIALES.PRC_CODTSE IS 'C�digo de servicio';

COMMENT ON COLUMN GESCOM.PROVEEDOR_CREDENCIALES.PRC_CODSIS IS 'C�digo del sistema externo';

COMMENT ON COLUMN GESCOM.PROVEEDOR_CREDENCIALES.PRC_CODMCE IS 'C�digo modelo de compra';

COMMENT ON COLUMN GESCOM.PROVEEDOR_CREDENCIALES.PRC_CODMLU IS 'C�digo usuario responsable';

COMMENT ON COLUMN GESCOM.PROVEEDOR_CREDENCIALES.PRC_CODCPC IS 'C�digo de las preferencias del cliente';

COMMENT ON COLUMN GESCOM.PROVEEDOR_CREDENCIALES.PRC_JSON IS 'Json del proveedor de la credencial';


CREATE UNIQUE INDEX GESCOM.PRC_PK_I ON GESCOM.PROVEEDOR_CREDENCIALES
(PRC_CODPRC)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );
CREATE UNIQUE INDEX GESCOM.PRC_UK ON GESCOM.PROVEEDOR_CREDENCIALES
(PRC_CODSIS)
LOGGING
TABLESPACE NEWTOUR_DAT02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE GESCOM.PROVEEDOR_CREDENCIALES ADD (
  CONSTRAINT PRC_PK
  PRIMARY KEY
  (PRC_CODPRC)
  USING INDEX GESCOM.PRC_PK_I
  ENABLE VALIDATE
,  CONSTRAINT PRC_UK
  UNIQUE (PRC_CODSIS)
  USING INDEX GESCOM.PRC_UK
  ENABLE VALIDATE);


CREATE INDEX GESCOM.PRC_CPC_FK_I ON GESCOM.PROVEEDOR_CREDENCIALES
(PRC_CODCPC)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX GESCOM.PRC_MCE_FK_I ON GESCOM.PROVEEDOR_CREDENCIALES
(PRC_CODMCE)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX GESCOM.PRC_MLU_FK_I ON GESCOM.PROVEEDOR_CREDENCIALES
(PRC_CODMLU)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE INDEX GESCOM.PRC_TSE_FK_I ON GESCOM.PROVEEDOR_CREDENCIALES
(PRC_CODTSE)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

CREATE OR REPLACE PUBLIC SYNONYM PROVEEDOR_CREDENCIALES FOR GESCOM.PROVEEDOR_CREDENCIALES;


ALTER TABLE GESCOM.PROVEEDOR_CREDENCIALES ADD (
  CONSTRAINT PRC_CPC_FK 
  FOREIGN KEY (PRC_CODCPC) 
  REFERENCES GESCOM.CLIENT_PREF_CREDENCIALES (CPC_CODCPC)
  ENABLE VALIDATE
,  CONSTRAINT PRC_MCE_FK 
  FOREIGN KEY (PRC_CODMCE) 
  REFERENCES GESCOM.MODELO_COMPRA_CREDENCIALES (MCE_CODMCE)
  ENABLE VALIDATE
,  CONSTRAINT PRC_MLU_FK 
  FOREIGN KEY (PRC_CODMLU) 
  REFERENCES NWTMNU.MNU_LOGIN_USER (MLU_ID)
  ENABLE VALIDATE
,  CONSTRAINT PRC_TSE_FK 
  FOREIGN KEY (PRC_CODTSE) 
  REFERENCES GESCOM.TIPO_SERVICIO_CREDENCIALES (TSE_CODTSE)
  ENABLE VALIDATE);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON GESCOM.PROVEEDOR_CREDENCIALES TO PUBLIC;
