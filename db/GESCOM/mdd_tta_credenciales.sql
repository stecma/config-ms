DROP TABLE GESCOM.MDD_TTA_CREDENCIALES CASCADE CONSTRAINTS;

CREATE TABLE GESCOM.MDD_TTA_CREDENCIALES
(
  MTT_CODMDD  VARCHAR2(6 BYTE)                  NOT NULL,
  MTT_CODTTA  VARCHAR2(6 BYTE)                  NOT NULL
)
TABLESPACE NEWTOUR_DAT02
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE;

COMMENT ON TABLE GESCOM.MDD_TTA_CREDENCIALES IS 'Tabla auxiliar que deshace la relacion N:M entre los modelos de distribuci�n y los tipos de tarifa de las credenciales';

COMMENT ON COLUMN GESCOM.MDD_TTA_CREDENCIALES.MTT_CODMDD IS 'C�digo modelo distribuci�n';

COMMENT ON COLUMN GESCOM.MDD_TTA_CREDENCIALES.MTT_CODTTA IS 'C�digo tipo de tarifa';


CREATE UNIQUE INDEX GESCOM.MTT_PK_I ON GESCOM.MDD_TTA_CREDENCIALES
(MTT_CODMDD, MTT_CODTTA)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE GESCOM.MDD_TTA_CREDENCIALES ADD (
  CONSTRAINT MTT_PK
  PRIMARY KEY
  (MTT_CODMDD, MTT_CODTTA)
  USING INDEX GESCOM.MTT_PK_I
  ENABLE VALIDATE);


CREATE OR REPLACE PUBLIC SYNONYM MDD_TTA_CREDENCIALES FOR GESCOM.MDD_TTA_CREDENCIALES;


ALTER TABLE GESCOM.MDD_TTA_CREDENCIALES ADD (
  CONSTRAINT MDD_MTT_FK 
  FOREIGN KEY (MTT_CODMDD) 
  REFERENCES GESCOM.MODELO_DISTRIB_CREDENCIALES (MDD_CODMDD)
  ENABLE VALIDATE
,  CONSTRAINT TTA_MTT_FK 
  FOREIGN KEY (MTT_CODTTA) 
  REFERENCES GESCOM.TIPOS_TARIFA_CREDENCIALES (TTA_CODTTA)
  ENABLE VALIDATE);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON GESCOM.MDD_TTA_CREDENCIALES TO PUBLIC;
