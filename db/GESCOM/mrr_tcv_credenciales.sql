DROP TABLE GESCOM.MRR_TCV_CREDENCIALES CASCADE CONSTRAINTS;

CREATE TABLE GESCOM.MRR_TCV_CREDENCIALES
(
  MTC_CODMRR  VARCHAR2(6 BYTE)                  NOT NULL,
  MTC_CODTCV  VARCHAR2(6 BYTE)                  NOT NULL
)
TABLESPACE NEWTOUR_DAT02
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE;

COMMENT ON TABLE GESCOM.MRR_TCV_CREDENCIALES IS 'Tabla auxiliar que deshace la relacion N:M entre las marcas y la distribucion de los tipos de servicio de las credenciales';

COMMENT ON COLUMN GESCOM.MRR_TCV_CREDENCIALES.MTC_CODMRR IS 'Identificador de la marca de la credencial';

COMMENT ON COLUMN GESCOM.MRR_TCV_CREDENCIALES.MTC_CODTCV IS 'Identificador de la distribucion de los tipos de servicio de la credencial';


CREATE UNIQUE INDEX GESCOM.MTC_PK_I ON GESCOM.MRR_TCV_CREDENCIALES
(MTC_CODMRR, MTC_CODTCV)
LOGGING
TABLESPACE NEWTOUR_IND02
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

ALTER TABLE GESCOM.MRR_TCV_CREDENCIALES ADD (
  CONSTRAINT MTC_PK
  PRIMARY KEY
  (MTC_CODMRR, MTC_CODTCV)
  USING INDEX GESCOM.MTC_PK_I
  ENABLE VALIDATE);


CREATE OR REPLACE PUBLIC SYNONYM MRR_TCV_CREDENCIALES FOR GESCOM.MRR_TCV_CREDENCIALES;


ALTER TABLE GESCOM.MRR_TCV_CREDENCIALES ADD (
  CONSTRAINT MRR_MTC_FK 
  FOREIGN KEY (MTC_CODMRR) 
  REFERENCES NWT00.MARCAS_RESERVA (MRR_CODMRR)
  ENABLE VALIDATE
,  CONSTRAINT TCV_MTC_FK 
  FOREIGN KEY (MTC_CODTCV) 
  REFERENCES GESCOM.SERVICIO_DISTRIB_CREDENCIALES (TCV_CODTCV)
  ENABLE VALIDATE);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON GESCOM.MRR_TCV_CREDENCIALES TO PUBLIC;
