SET DEFINE OFF;
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('439ab0e3-0fe4-4cb4-93ba-111d2deb3564', '5872f7f0-99a9-4b4a-bdf2-f9d16386a75a', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'GCT', 'RE', 
    '{"id":"439ab0e3-0fe4-4cb4-93ba-111d2deb3564","act":false,"del":false,"user":"test","pass":"test","bookFrom":"18/10/2020","timeBkFrom":"00:00:00","bookTo":"29/11/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["GCT"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false,"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41","groupId":"5872f7f0-99a9-4b4a-bdf2-f9d16386a75a"}', TO_DATE('23/10/2020 12:54:20', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('11/11/2020 11:19:15', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('d2583f88-ecda-4d15-bccd-00deeff68780', '5872f7f0-99a9-4b4a-bdf2-f9d16386a75a', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'ISD', 'RE', 
    '{"id":"d2583f88-ecda-4d15-bccd-00deeff68780","act":false,"del":false,"user":"test","pass":"test","bookFrom":"17/11/2020","timeBkFrom":"00:00:00","bookTo":"30/11/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["ISD"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false,"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41","groupId":"5872f7f0-99a9-4b4a-bdf2-f9d16386a75a"}', TO_DATE('09/11/2020 13:36:52', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('11/11/2020 11:19:15', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('e8aef557-8733-40f6-8515-8280248b90e4', '5872f7f0-99a9-4b4a-bdf2-f9d16386a75a', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'GFR', 'RE', 
    '{"id":"e8aef557-8733-40f6-8515-8280248b90e4","act":false,"del":true,"user":"Alex","pass":"Alex","checkInFrom":"17/09/2020","timeInFrom":"00:00:00","checkInTo":"30/09/2020","timeInTo":"23:59:59","bookFrom":"01/09/2020","timeBkFrom":"00:00:00","bookTo":"26/10/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["GFR"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false}', TO_DATE('06/08/2020 11:50:43', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('07/08/2020 12:14:55', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('e31beb7e-aeaf-45d1-889a-b531f09eb091', '7cff2137-9b06-4e4c-ba83-4a5a1610d479', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'TRP', 'RE', 
    '{"id":"e31beb7e-aeaf-45d1-889a-b531f09eb091","act":false,"del":false,"user":"Alex","pass":"Alex","checkInFrom":"23/09/2020","bookFrom":"01/09/2020","timeBkFrom":"00:00:00","bookTo":"30/11/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["TRP"]},{"key":"BILLING_TYPE","value":["RE"]}],"connect":"<User>ALEX</User><Password>ALEX</Password><UrlGeneric>http://hb2b.travelgatex.com</UrlGeneric><Parameters><Parameter key = ''accessToken'' value = ''Rc/5FrJPYjyzuEppORnlVWZlfkB8qGRI07jb3RWyj72NKopKKSNbM/CD+RIiZyLpGj29LDw4mH6Y99weSJBjurBYE+yHaWoIrBarJSsyHmY=''/><Parameter key = ''HotelXAccessCode'' value = ''4224''/></Parameters>","new":false,"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41","groupId":"7cff2137-9b06-4e4c-ba83-4a5a1610d479"}', TO_DATE('06/08/2020 11:50:43', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('11/11/2020 11:19:15', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('e8a2508a-ae93-4fbc-9cb2-440c1d410a8e', '5872f7f0-99a9-4b4a-bdf2-f9d16386a75a', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'TRP', 'RE', 
    '{"id":"e8a2508a-ae93-4fbc-9cb2-440c1d410a8e","act":false,"del":true,"user":"usuario2","pass":"pass2","checkInFrom":"01/10/2020","timeInFrom":"00:00:00","checkInTo":"31/10/2020","timeInTo":"23:59:59","bookFrom":"11/08/2020","timeBkFrom":"00:00:00","bookTo":"21/08/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["TRP"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false}', TO_DATE('14/08/2020 8:30:15', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('14/08/2020 8:30:15', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('22359bca-c981-42b9-94d4-1a702eaeafd9', '5872f7f0-99a9-4b4a-bdf2-f9d16386a75a', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'TRP', 'RE', 
    '{"id":"22359bca-c981-42b9-94d4-1a702eaeafd9","act":false,"del":true,"user":"usuario1","pass":"pass1","checkInFrom":"01/09/2020","timeInFrom":"00:00:00","checkInTo":"30/09/2020","timeInTo":"23:59:59","bookFrom":"11/08/2020","timeBkFrom":"00:00:00","bookTo":"21/08/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["TRP"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false}', TO_DATE('14/08/2020 8:30:15', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('14/08/2020 8:30:15', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('19615c32-de5d-4367-904d-5cd4b3cef82d', '5872f7f0-99a9-4b4a-bdf2-f9d16386a75a', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'TRP', 'RE', 
    '{"id":"19615c32-de5d-4367-904d-5cd4b3cef82d","act":false,"del":false,"user":"usu123","pass":"pass123","bookFrom":"11/08/2020","timeBkFrom":"12:00:00","bookTo":"22/08/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["TRP"]},{"key":"BILLING_TYPE","value":["RE"]}],"connect":"<User>57037</User><Password></Password><UrlGeneric>http://hb2b.travelgatex.com</UrlGeneric><Parameters><Parameter key = ''accessToken'' value = ''Rc/5FrJPYjyzuEppORnlVWZlfkB8qGRI07jb3RWyj72NKopKKSNbM/CD+RIiZyLpGj29LDw4mH6Y99weSJBjurBYE+yHaWoIrBarJSsyHmY=''/><Parameter key = ''HotelXAccessCode'' value = ''4224''/></Parameters>","new":false,"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41","groupId":"5872f7f0-99a9-4b4a-bdf2-f9d16386a75a"}', TO_DATE('14/08/2020 9:14:12', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('11/11/2020 11:19:15', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('7b173081-61c2-4c94-ae9c-4c9ff7ea6bdc', '7cff2137-9b06-4e4c-ba83-4a5a1610d479', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'GCT', 'RE', 
    '{"id":"7b173081-61c2-4c94-ae9c-4c9ff7ea6bdc","act":false,"del":false,"user":"user","pass":"pass","checkInFrom":"01/09/2020","bookFrom":"31/08/2020","timeBkFrom":"00:00:00","bookTo":"30/11/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["GCT"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false,"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41","groupId":"7cff2137-9b06-4e4c-ba83-4a5a1610d479"}', TO_DATE('26/08/2020 9:56:33', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('11/11/2020 11:19:15', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('e4dadfab-dbff-4792-8907-07d18317a813', 'df894026-6b16-49d6-89f5-ef913248127f', '4f293b7d-64b7-4f65-aace-037ce95e1107', 'TRP', 'RE', 
    '{"id":"e4dadfab-dbff-4792-8907-07d18317a813","act":false,"del":false,"user":"PRUEBAHORAS","pass":"PRUEBAHORAS","checkInFrom":"28/08/2020","timeInFrom":"00:00:00","checkInTo":"31/10/2020","timeInTo":"12:00:00","bookFrom":"28/08/2020","timeBkFrom":"00:00:00","bookTo":"31/10/2020","timeBkTo":"12:00:00","codes":[{"key":"CORPORATION","value":["TRP"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false,"supplierId":"4f293b7d-64b7-4f65-aace-037ce95e1107","groupId":"df894026-6b16-49d6-89f5-ef913248127f"}', TO_DATE('28/08/2020 9:58:32', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('02/09/2020 15:14:35', 'DD/MM/YYYY HH24:MI:SS'));
Insert into GESCOM.EXTERNAL_SIS_CREDENCIALES
   (ESC_CODESC, ESC_CODSEC, ESC_CODPRC, ESC_CODSOC, ESC_CODTFC, 
    ESC_JSON, ESC_CREATED, ESC_UPDATED)
 Values
   ('0633673a-3408-4b53-8705-9efe24b8da9a', 'df894026-6b16-49d6-89f5-ef913248127f', '4f293b7d-64b7-4f65-aace-037ce95e1107', 'TRP', 'RE', 
    '{"id":"0633673a-3408-4b53-8705-9efe24b8da9a","act":false,"del":false,"user":"PRUEBAHORAS","pass":"PRUEBAHORAS","checkInFrom":"31/10/2020","timeInFrom":"12:01:00","checkInTo":"31/12/2020","timeInTo":"23:59:59","bookFrom":"31/10/2020","timeBkFrom":"12:01:00","bookTo":"31/12/2020","timeBkTo":"23:59:59","codes":[{"key":"CORPORATION","value":["TRP"]},{"key":"BILLING_TYPE","value":["RE"]}],"new":false,"supplierId":"4f293b7d-64b7-4f65-aace-037ce95e1107","groupId":"df894026-6b16-49d6-89f5-ef913248127f"}', TO_DATE('28/08/2020 13:03:21', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('02/09/2020 15:14:35', 'DD/MM/YYYY HH24:MI:SS'));
COMMIT;
