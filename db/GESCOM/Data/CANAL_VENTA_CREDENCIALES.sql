SET DEFINE OFF;
Insert into GESCOM.CANAL_VENTA_CREDENCIALES
   (CVC_CODCVC, CVC_NOMCVC, CVC_JSON)
 Values
   ('WEB', '[WEB] Web', '{"id":"WEB","name":[{"key":"ESP","value":"[WEB] Web"}],"act":true,"del":false,"entity":"SALES_CHANNEL","allMasters":[],"codes":[{"key":"DISTRIBUTION_MODEL","value":["BTA"]},{"key":"ENVIRONMENTS","value":["NVIP","VIP"]},{"key":"REQUEST","value":["MONO","MULTI","DEST","HIBR"]},{"key":"NUM_CLIENTS","value":["MULTI"]}]}');
Insert into GESCOM.CANAL_VENTA_CREDENCIALES
   (CVC_CODCVC, CVC_NOMCVC, CVC_JSON)
 Values
   ('API','[API] Api','{"id":"API","name":[{"key":"ESP","value":"[API] Api"}],"act":true,"del":false,"entity":"SALES_CHANNEL","allMasters":[],"codes":[{"key":"ENVIRONMENTS","value":["NVIP","VIP"]},{"key":"REQUEST","value":["HIBR","MONO","MULTI","DEST"]},{"key":"DISTRIBUTION_MODEL","value":["B2C","CFL","BTA"]},{"key":"NUM_CLIENTS","value":["UNICO"]}]}');
Insert into GESCOM.CANAL_VENTA_CREDENCIALES
   (CVC_CODCVC, CVC_NOMCVC, CVC_JSON)
 Values
   ('NWW','[NWW] Newtour Web','{"id":"NWW","name":[{"key":"ESP","value":"[NWW] Newtour Web"}],"act":true,"del":false,"entity":"SALES_CHANNEL","allMasters":["request","environments"],"masters":[],"codes":[{"key":"NUM_CLIENTS","value":["MULTI"]}]}');
COMMIT;


