SET DEFINE OFF;
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('B2C', 'VINC');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('BTA', 'B2B');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('BTA', 'EMP');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('BTA', 'VINC');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CFL', 'VINC');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CHO', 'VINC');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CLM', 'VINC');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CMO', 'VINC');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CPM', 'VINC');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CPQ', 'B2B');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CPQ', 'EMP');
Insert into GESCOM.MDD_TTA_CREDENCIALES
   (MTT_CODMDD, MTT_CODTTA)
 Values
   ('CPQ', 'VINC');
COMMIT;
