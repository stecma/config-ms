SET DEFINE OFF;
Insert into GESCOM.TIPO_FACTURACION_CREDENCIALES
   (TFC_CODTFC, TFC_NOMTFC, TFC_JSON)
 Values
   ('RG', '[RG] R�gimen general', '{"id":"RG","name":[{"key":"ESP","value":"[RG] R�gimen general"}],"act":true,"del":false,"entity":"BILLING_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TIPO_FACTURACION_CREDENCIALES
   (TFC_CODTFC, TFC_NOMTFC, TFC_JSON)
 Values
   ('RE', '[RE] R�gimen especial', '{"id":"RE","name":[{"key":"ESP","value":"[RE] R�gimen especial"}],"act":true,"del":false,"entity":"BILLING_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
