SET DEFINE OFF;
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('EBUS', '[EBUS] EBUSINESS', '{"id":"EBUS","name":[{"key":"ESP","value":"[EBUS] EBUSINESS"}],"act":true,"del":false,"entity":"DEPTH","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('NAC', '[NAC] Comercial Nacional', '{"entity": "DEPTH","id": "NAC","act":true,"del":false,"name": [{"key": "ESP","value": "[NAC] Comercial Nacional"}]}');
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('EMEA', '[EMEA] Comercial Emea', '{"id":"EMEA","name":[{"key":"ESP","value":"[EMEA] Comercial Emea"}],"act":true,"del":false,"entity":"DEPTH","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"addrem":false,"sendrem":false,"shwrate":false,"supByZone":false,"params":[{"name":"EXP","value":"4"},{"name":"ALEX","value":"4"},{"name":"HTL","value":"4"},{"name":"TRF","value":"4"}]}');
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('LATAM', '[LATAM] Comercial LATAM', '{"entity": "DEPTH","id": "LATAM","act":true,"del":false,"name": [{"key": "ESP","value": "[LATAM] Comercial LATAM"}]}');
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('CYP', '[CYP] Contratación y Producto', '{"id":"CYP","name":[{"key":"ESP","value":"[CYP] Contratación y Producto"}],"act":true,"del":false,"entity":"DEPTH","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"addrem":false,"sendrem":false,"shwrate":false,"supByZone":false,"params":[{"name":"EXP","value":"2"},{"name":"ALEX","value":"1"},{"name":"TRF","value":"2"},{"name":"HTL","value":"2"}]}');
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('FYS', '[FYS] Formación y Soporte', '{"entity": "DEPTH","id": "FYS","act":true,"del":false,"name": [{"key": "ESP","value": "[FYS] Formación y Soporte"}]}');
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('REV', '[REV] Revenue', '{"entity": "DEPTH","id": "REV","act":true,"del":false,"name": [{"key": "ESP","value": "[REV] Revenue"}]}');
Insert into GESCOM.DEPARTAMENTOS_CREDENCIALES
   (DEP_CODDEP, DEP_NOMDEP, DEP_JSON)
 Values
   ('MARSOL', '[MARSOL] Marsol', '{"entity": "DEPTH","id": "MARSOL","act":true,"del":false,"name": [{"key": "ESP","value": "[MARSOL] Marsol"}]}');
COMMIT;
