SET DEFINE OFF;
Insert into GESCOM.ENTORNOS_CREDENCIALES
   (ENT_CODENT, ENT_NOMENT, ENT_JSON)
 Values
   ('NVIP', '[NVIP] No VIP', '{"id":"NVIP","name":[{"key":"ESP","value":"[NVIP] No VIP"}],"act":true,"del":false,"entity":"ENVIRONMENTS","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.ENTORNOS_CREDENCIALES
   (ENT_CODENT, ENT_NOMENT, ENT_JSON)
 Values
   ('VIP', '[VIP] VIP', '{"id":"VIP","name":[{"key":"ESP","value":"[VIP] VIP"}],"act":true,"del":false,"entity":"ENVIRONMENTS","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
