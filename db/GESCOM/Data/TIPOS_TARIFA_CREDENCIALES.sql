SET DEFINE OFF;
Insert into GESCOM.TIPOS_TARIFA_CREDENCIALES
   (TTA_CODTTA, TTA_NOMTTA, TTA_JSON)
 Values
   ('NWW', '[NWW] Exclusivo Newtour Web', '{"id":"NWW","name":[{"key":"ESP","value":"[NWW] Exclusivo Newtour Web"}],"act":true,"del":false,"entity":"RATE_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"ratecat":"CUSTOMER"}');
Insert into GESCOM.TIPOS_TARIFA_CREDENCIALES
   (TTA_CODTTA, TTA_NOMTTA, TTA_JSON)
 Values
   ('VINC', '[VINC] Vinculante', '{"id":"VINC","name":[{"key":"ESP","value":"[VINC] Vinculante"}],"act":true,"del":false,"entity":"RATE_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"ratecat":"BOTH"}');
Insert into GESCOM.TIPOS_TARIFA_CREDENCIALES
   (TTA_CODTTA, TTA_NOMTTA, TTA_JSON)
 Values
   ('B2B', '[B2B] B2B', '{"id":"B2B","name":[{"key":"ESP","value":"[B2B] B2B"}],"act":true,"del":false,"entity":"RATE_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"ratecat":"BOTH","addrem":false,"sendrem":false,"shwrate":false,"supByZone":false}');
Insert into GESCOM.TIPOS_TARIFA_CREDENCIALES
   (TTA_CODTTA, TTA_NOMTTA, TTA_JSON)
 Values
   ('EMP', '[EMP] Empaquetar', '{"id":"EMP","name":[{"key":"ESP","value":"[EMP] Empaquetar"}],"act":true,"del":false,"entity":"RATE_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"ratecat":"BOTH"}');
Insert into GESCOM.TIPOS_TARIFA_CREDENCIALES
   (TTA_CODTTA, TTA_NOMTTA, TTA_JSON)
 Values
   ('INTER', '[INTER] Interline', '{"id":"INTER","name":[{"key":"ESP","value":"[INTER] Interline"}],"act":true,"del":false,"entity":"RATE_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"ratecat":"CUSTOMER"}');
Insert into GESCOM.TIPOS_TARIFA_CREDENCIALES
   (TTA_CODTTA, TTA_NOMTTA, TTA_JSON)
 Values
   ('NRF', '[NRF] No Reembolsable', '{"id":"NRF","name":[{"key":"ESP","value":"[NRF] No Reembolsable"}],"act":true,"del":false,"entity":"RATE_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"ratecat":"BOTH"}');
Insert into GESCOM.TIPOS_TARIFA_CREDENCIALES
   (TTA_CODTTA, TTA_NOMTTA, TTA_JSON)
 Values
   ('RQ', '[RQ] Bajo Petici�n', '{"id":"RQ","name":[{"key":"ESP","value":"[RQ] Bajo Petici�n"}],"act":true,"del":false,"entity":"RATE_TYPE","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"ratecat":"BOTH"}');
COMMIT;
