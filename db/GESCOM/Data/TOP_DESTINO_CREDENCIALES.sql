SET DEFINE OFF;
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('USA', '[USA] USA', '{"id":"USA","name":[{"key":"ESP","value":"[USA] USA"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('CARIB', '[CARIB] CARIBE', '{"id":"CARIB","name":[{"key":"ESP","value":"[CARIB] CARIBE"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('ESP', '[ESP] ESPA�A', '{"id":"ESP","name":[{"key":"ESP","value":"[ESP] ESPA�A"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('PRT', '[PRT] PORTUGAL', '{"id":"PRT","name":[{"key":"ESP","value":"[PRT] PORTUGAL"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('ITL', '[ITL] ITALIA', '{"id":"ITL","name":[{"key":"ESP","value":"[ITL] ITALIA"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('UK', '[UK] UK', '{"id":"UK","name":[{"key":"ESP","value":"[UK] UK"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('FRC', '[FRC] FRANCIA', '{"id":"FRC","name":[{"key":"ESP","value":"[FRC] FRANCIA"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('MEDIT', '[MEDIT] MEDITERRANEO (GR, MT, TR, HR)', '{"id":"MEDIT","name":[{"key":"ESP","value":"[MEDIT] MEDITERRANEO (GR, MT, TR, HR)"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('EUROP', '[EUROP] RESTO EUROPA', '{"id":"EUROP","name":[{"key":"ESP","value":"[EUROP] RESTO EUROPA"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('ORMED', '[ORMED]  ORIENTE MEDIO', '{"id":"ORMED","name":[{"key":"ESP","value":"[ORMED]  ORIENTE MEDIO"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('MRC', '[MRC] MARRUECOS', '{"id":"MRC","name":[{"key":"ESP","value":"[MRC] MARRUECOS"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('AFRIC', '[AFRIC] RESTO �FRICA', '{"id":"AFRIC","name":[{"key":"ESP","value":"[AFRIC] RESTO �FRICA"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('ASIA', '[ASIA] ASIA', '{"id":"ASIA","name":[{"key":"ESP","value":"[ASIA] ASIA"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('OCEAN', '[OCEAN] OCEAN�A', '{"id":"OCEAN","name":[{"key":"ESP","value":"[OCEAN] OCEAN�A"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TOP_DESTINO_CREDENCIALES
   (TDC_CODTDC, TDC_NOMTDC, TDC_JSON)
 Values
   ('LATAM', '[LATAM] Latam', '{"id":"LATAM","name":[{"key":"ESP","value":"[LATAM] Latam"}],"act":true,"del":false,"entity":"TOP_DESTINATION","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
