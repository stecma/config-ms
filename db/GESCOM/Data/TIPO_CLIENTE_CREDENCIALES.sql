SET DEFINE OFF;
Insert into GESCOM.TIPO_CLIENTE_CREDENCIALES
   (TCL_CODTCL, TCL_NOMTCL, TCL_JSON)
 Values
   ('B2C', '[B2C] B2C', '{"id":"B2C","name":[{"key":"ESP","value":"[B2C] B2C"}],"act":true,"del":false,"entity":"CLIENT_TYPE","allMasters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"DISTRIBUTION_MODEL","value":["CPM","CMO","CLM","CPQ","CFL","CHO"]}]}');
Insert into GESCOM.TIPO_CLIENTE_CREDENCIALES
   (TCL_CODTCL, TCL_NOMTCL, TCL_JSON)
 Values
   ('B2B', '[B2B] B2B', '{"id":"B2B","name":[{"key":"ESP","value":"[B2B] B2B"}],"act":true,"del":false,"entity":"CLIENT_TYPE","allMasters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"DISTRIBUTION_MODEL","value":["BTA"]}]}');
Insert into GESCOM.TIPO_CLIENTE_CREDENCIALES
   (TCL_CODTCL, TCL_NOMTCL, TCL_JSON)
 Values
   ('B2B2C', '[B2B2C] B2B2C', '{"id":"B2B2C","name":[{"key":"ESP","value":"[B2B2C] B2B2C"}],"act":true,"del":false,"entity":"CLIENT_TYPE","allMasters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"DISTRIBUTION_MODEL","value":["B2C"]}]}');
COMMIT;
