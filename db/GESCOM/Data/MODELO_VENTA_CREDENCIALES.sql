SET DEFINE OFF;
Insert into GESCOM.MODELO_VENTA_CREDENCIALES
   (MVE_CODMVE, MVE_NOMMVE, MVE_JSON)
 Values
   ('N', '[N] Netos', '{"id":"N","name":[{"key":"ESP","value":"[N] Netos"}],"act":true,"del":false,"entity":"SALES_MODEL","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.MODELO_VENTA_CREDENCIALES
   (MVE_CODMVE, MVE_NOMMVE, MVE_JSON)
 Values
   ('C', '[C] Comisionable', '{"id":"C","name":[{"key":"ESP","value":"[C] Comisionable"}],"act":true,"del":false,"entity":"SALES_MODEL","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.MODELO_VENTA_CREDENCIALES
   (MVE_CODMVE, MVE_NOMMVE, MVE_JSON)
 Values
   ('X', '[X] Neto + Comisionable', '{"id":"X","name":[{"key":"ESP","value":"[X] Neto + Comisionable"}],"act":true,"del":false,"entity":"SALES_MODEL","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
