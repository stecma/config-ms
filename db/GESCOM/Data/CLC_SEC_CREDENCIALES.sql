SET DEFINE OFF;
Insert into GESCOM.CLC_SEC_CREDENCIALES
   (CSC_CODCLC, CSC_CODSEC, CSC_CODPRC, CSC_JSON)
 Values
   ('9ff17c29-499b-4441-ae2e-f231e9fbaff0', '7cff2137-9b06-4e4c-ba83-4a5a1610d479', 'de19d846-1b5e-470d-be28-76bdbfa64a41', '{"id":"7cff2137-9b06-4e4c-ba83-4a5a1610d479","act":true,"del":false,"sysname":"MAR_B2B_SH_ALL_MULTI","test":false,"dft":false,"hotX":"AMBOS","tagdiv":true,"tagnac":false,"allMasters":["distribution_model"],"divs":[],"codes":[{"key":"RATE_TYPE","value":["B2B"]},{"key":"CLIENT_TYPE","value":["B2B"]},{"key":"COUNTRY�exc","value":["AL","DE"]},{"key":"BRAND","value":["MAR"]}],"new":false,"assignment":{"checkInFrom":"30/10/2020","checkInTo":"03/11/2020","bookFrom":"29/10/2020","timeBkFrom":"00:00:00","bookTo":"04/11/2020"},"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41"}');
Insert into GESCOM.CLC_SEC_CREDENCIALES
   (CSC_CODCLC, CSC_CODSEC, CSC_CODPRC, CSC_JSON)
 Values
   ('96dc6ad9-0812-409d-a4e8-14972cb05599', '7cff2137-9b06-4e4c-ba83-4a5a1610d479', 'de19d846-1b5e-470d-be28-76bdbfa64a41', '{"id":"7cff2137-9b06-4e4c-ba83-4a5a1610d479","act":true,"del":false,"sysname":"MAR_B2B_SH_ALL_MULTI","test":false,"dft":false,"hotX":"AMBOS","tagdiv":true,"tagnac":false,"allMasters":["distribution_model"],"divs":[],"codes":[{"key":"RATE_TYPE","value":["B2B"]},{"key":"CLIENT_TYPE","value":["B2B"]},{"key":"COUNTRY�exc","value":["AL","DE"]},{"key":"BRAND","value":["MAR"]}],"new":false,"assignment":{"checkInFrom":"30/10/2020","bookFrom":"29/10/2020","timeBkFrom":"00:00:00","note":[{"user":"Alejandro Moya (34064792K)","date":"30/10/2020 08:58:41","note":"Nota credencial"}]},"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41"}');
Insert into GESCOM.CLC_SEC_CREDENCIALES
   (CSC_CODCLC, CSC_CODSEC, CSC_CODPRC, CSC_JSON)
 Values
   ('12911ecd-e085-4e93-b220-ca952486d5f0', '7cff2137-9b06-4e4c-ba83-4a5a1610d479', 'de19d846-1b5e-470d-be28-76bdbfa64a41', '{"id":"7cff2137-9b06-4e4c-ba83-4a5a1610d479","act":true,"del":false,"sysname":"MAR_B2B_SH_ALL_MULTI","test":false,"dft":false,"hotX":"AMBOS","tagdiv":true,"tagnac":false,"allMasters":["distribution_model"],"divs":[],"codes":[{"key":"RATE_TYPE","value":["B2B"]},{"key":"CLIENT_TYPE","value":["B2B"]},{"key":"COUNTRY�exc","value":["AL","DE"]},{"key":"BRAND","value":["MAR"]}],"new":false,"assignment":{"checkInFrom":"29/10/2020","bookFrom":"29/10/2020","timeBkFrom":"09:45:47","note":[{"user":"Alejandro Moya (34064792K)","date":"29/10/2020 09:45:52","note":"Asignaci�n"}]},"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41"}');
COMMIT;
