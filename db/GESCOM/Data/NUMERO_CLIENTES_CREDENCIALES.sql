SET DEFINE OFF;
Insert into GESCOM.NUM_CLIENTES_CREDENCIALES
   (NCL_CODNCL,NCL_NOMNCL,NCL_JSON)
 Values
   ('UNICO','UNICO','{"id":"UNICO","name":[{"key":"ESP","value":"UNICO"}],"act":true,"del":false,"entity":"NUM_CLIENTS","allMasters":[],"masters":[]}');
Insert into GESCOM.NUM_CLIENTES_CREDENCIALES
   (NCL_CODNCL,NCL_NOMNCL,NCL_JSON)
 Values
   ('MULTI','MULTI','{"id":"MULTI","name":[{"key":"ESP","value":"MULTI"}],"act":true,"del":false,"entity":"NUM_CLIENTS","allMasters":[],"masters":[]}');
COMMIT;