SET DEFINE OFF;
Insert into GESCOM.MODELO_COMPRA_CREDENCIALES
   (MCE_CODMCE, MCE_NOMMCE, MCE_JSON)
 Values
   ('X', '[X] Netos + Comisionables', '{"id":"X","name":[{"key":"ESP","value":"[X] Netos + Comisionables"}],"act":true,"del":true,"entity":"buy_model","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.MODELO_COMPRA_CREDENCIALES
   (MCE_CODMCE, MCE_NOMMCE, MCE_JSON)
 Values
   ('C', '[C] Comisionable', '{"id":"C","name":[{"key":"ESP","value":"[C] Comisionable"}],"act":true,"del":false,"entity":"BUY_MODEL","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.MODELO_COMPRA_CREDENCIALES
   (MCE_CODMCE, MCE_NOMMCE, MCE_JSON)
 Values
   ('N', '[N] Netos', '{"id":"N","name":[{"key":"ESP","value":"[N] Netos"}],"act":true,"del":false,"entity":"BUY_MODEL","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
