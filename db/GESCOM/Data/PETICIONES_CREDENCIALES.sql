SET DEFINE OFF;
Insert into GESCOM.PETICIONES_CREDENCIALES
   (PET_CODPET, PET_NOMPET, PET_JSON)
 Values
   ('MONO', '[MONO] Mono - Hotel', '{"id":"MONO","name":[{"key":"ESP","value":"[MONO] Mono - Hotel"}],"act":true,"del":false,"entity":"REQUEST","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PETICIONES_CREDENCIALES
   (PET_CODPET, PET_NOMPET, PET_JSON)
 Values
   ('MULTI', '[MULTI] Multi- Hotel', '{"id":"MULTI","name":[{"key":"ESP","value":"[MULTI] Multi- Hotel"}],"act":true,"del":false,"entity":"REQUEST","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PETICIONES_CREDENCIALES
   (PET_CODPET, PET_NOMPET, PET_JSON)
 Values
   ('DEST', '[DEST] Destino', '{"id":"DEST","name":[{"key":"ESP","value":"[DEST] Destino"}],"act":true,"del":false,"entity":"REQUEST","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PETICIONES_CREDENCIALES
   (PET_CODPET, PET_NOMPET, PET_JSON)
 Values
   ('HIBR', '[HIBR] H�brido', '{"id":"HIBR","name":[{"key":"ESP","value":"[HIBR] H�brido"}],"act":true,"del":false,"entity":"REQUEST","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
