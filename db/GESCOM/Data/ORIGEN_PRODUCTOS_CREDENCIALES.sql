SET DEFINE OFF;
Insert into GESCOM.ORIGEN_PRODUCTOS_CREDENCIALES
   (OPD_CODOPD, OPD_NOMOPD, OPD_JSON)
 Values
   ('PPNWT', '[PPNWT] Producto Propio Newtour', '{"id":"PPNWT","name":[{"key":"ESP","value":"[PPNWT] Producto Propio Newtour"}],"act":true,"del":false,"entity":"PRODUCT_ORIGIN","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.ORIGEN_PRODUCTOS_CREDENCIALES
   (OPD_CODOPD, OPD_NOMOPD, OPD_JSON)
 Values
   ('PPPULL', '[PPPULL] Producto Propio Pull', '{"id":"PPPULL","name":[{"key":"ESP","value":"[PPPULL] Producto Propio Pull"}],"act":true,"del":false,"entity":"PRODUCT_ORIGIN","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.ORIGEN_PRODUCTOS_CREDENCIALES
   (OPD_CODOPD, OPD_NOMOPD, OPD_JSON)
 Values
   ('MAR', '[MAR] Marsol Newtour', '{"id":"MAR","name":[{"key":"ESP","value":"[MAR] Marsol Newtour"}],"act":true,"del":false,"entity":"PRODUCT_ORIGIN","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.ORIGEN_PRODUCTOS_CREDENCIALES
   (OPD_CODOPD, OPD_NOMOPD, OPD_JSON)
 Values
   ('PULL3', '[PULL3] Producto Pull Terceros', '{"id":"PULL3","name":[{"key":"ESP","value":"[PULL3] Producto Pull Terceros"}],"act":true,"del":false,"entity":"PRODUCT_ORIGIN","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.ORIGEN_PRODUCTOS_CREDENCIALES
   (OPD_CODOPD, OPD_NOMOPD, OPD_JSON)
 Values
   ('DHIS', '[DHIS] Dhisco', '{"id":"DHIS","name":[{"key":"ESP","value":"[DHIS] Dhisco"}],"act":true,"del":false,"entity":"PRODUCT_ORIGIN","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
