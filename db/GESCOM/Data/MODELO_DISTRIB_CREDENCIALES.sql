SET DEFINE OFF;
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('CHO', '[CHO] B2C - Hotel Only', '{"id":"CHO","name":[{"key":"ESP","value":"[CHO] B2C - Hotel Only"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2C - Hotel Only Distribution","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC"]}]}');
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('CPQ', '[CPQ] B2C - Packaged', '{"id":"CPQ","name":[{"key":"ESP","value":"[CPQ] B2C - Packaged"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2C - Packaged Distribution","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["B2B","VINC","EMP"]}]}');
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('CPM', '[CPM] B2C - Membership', '{"id":"CPM","name":[{"key":"ESP","value":"[CPM] B2C - Membership"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2C - Membership Opaque Distribution","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC"]}]}');
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('CMO', '[CMO] B2C - Mobile', '{"id":"CMO","name":[{"key":"ESP","value":"[CMO] B2C - Mobile"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2C - Mobile App","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC"]}]}');
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('CFL', '[CFL] B2C - Flexi', '{"id":"CFL","name":[{"key":"ESP","value":"[CFL] B2C - Flexi"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2C - Flexi Distribution","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC"]}]}');
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('CLM', '[CLM] B2C - Last Minute', '{"id":"CLM","name":[{"key":"ESP","value":"[CLM] B2C - Last Minute"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2C - Last Minute Distribution","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC"]}]}');
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('BTA', '[BTA] B2B - Travel Agency', '{"id":"BTA","name":[{"key":"ESP","value":"[BTA] B2B - Travel Agency"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2B - High Street Shop","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["B2B","VINC","EMP"]}]}');
Insert into GESCOM.MODELO_DISTRIB_CREDENCIALES
   (MDD_CODMDD, MDD_NOMMDD, MDD_JSON)
 Values
   ('B2C', '[B2C] B2B2C - Bedbank', '{"id":"B2C","name":[{"key":"ESP","value":"[B2C] B2B2C - Bedbank"}],"act":true,"del":false,"entity":"DISTRIBUTION_MODEL","allMasters":[],"alias":"B2B2C - Wholesale Distribution","test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC"]}]}');
COMMIT;
