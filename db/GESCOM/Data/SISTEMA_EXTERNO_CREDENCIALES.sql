SET DEFINE OFF;
Insert into GESCOM.SISTEMA_EXTERNO_CREDENCIALES
   (SEC_CODSEC, SEC_CODPRC, SEC_NOMSEC, SEC_ACTIVO, SEC_TEST, 
    SEC_HOTELX, SEC_CREATED, SEC_UPDATED, SEC_JSON)
 Values
   ('5872f7f0-99a9-4b4a-bdf2-f9d16386a75a', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'WIS_B2C_SH_ALL_EUR', '1', '1', 
    'EXCLUSIVO', TO_DATE('06/08/2020 9:11:24', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('11/11/2020 11:19:15', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"5872f7f0-99a9-4b4a-bdf2-f9d16386a75a","act":true,"del":false,"sysname":"WIS_B2C_SH_ALL_EUR","test":true,"dft":false,"hotX":"EXCLUSIVO","tagdiv":false,"tagnac":false,"allMasters":[],"codes":[{"key":"RATE_TYPE","value":["B2B"]},{"key":"CLIENT_TYPE","value":["B2C"]},{"key":"DISTRIBUTION_MODEL","value":["CPM","CHO","CMO","CFL","CPQ","CLM"]},{"key":"COUNTRY�exc","value":["AL","DE"]},{"key":"CURRENCY","value":["EUR"]},{"key":"BRAND","value":["WIS"]}],"new":false,"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41"}');
Insert into GESCOM.SISTEMA_EXTERNO_CREDENCIALES
   (SEC_CODSEC, SEC_CODPRC, SEC_NOMSEC, SEC_ACTIVO, SEC_TEST, 
    SEC_HOTELX, SEC_CREATED, SEC_UPDATED, SEC_JSON)
 Values
   ('7cff2137-9b06-4e4c-ba83-4a5a1610d479', 'de19d846-1b5e-470d-be28-76bdbfa64a41', 'MAR_B2B_SH_ALL_MULTI', '1', '0', 
    'AMBOS', TO_DATE('06/08/2020 9:11:24', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('11/11/2020 11:19:15', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"7cff2137-9b06-4e4c-ba83-4a5a1610d479","act":true,"del":false,"sysname":"MAR_B2B_SH_ALL_MULTI","test":false,"dft":false,"hotX":"AMBOS","tagdiv":true,"tagnac":false,"allMasters":["distribution_model"],"divs":[],"codes":[{"key":"RATE_TYPE","value":["B2B"]},{"key":"CLIENT_TYPE","value":["B2B"]},{"key":"COUNTRY�exc","value":["AL","DE"]},{"key":"BRAND","value":["MAR"]}],"new":false,"supplierId":"de19d846-1b5e-470d-be28-76bdbfa64a41"}');
Insert into GESCOM.SISTEMA_EXTERNO_CREDENCIALES
   (SEC_CODSEC, SEC_CODPRC, SEC_NOMSEC, SEC_ACTIVO, SEC_TEST, 
    SEC_HOTELX, SEC_CREATED, SEC_UPDATED, SEC_JSON)
 Values
   ('d4db9949-5aee-4688-9245-2258afae8fee', '4f293b7d-64b7-4f65-aace-037ce95e1107', 'MAR_B2B_SH_ALL_MULTI', '1', '0', 
    'AMBOS', TO_DATE('28/08/2020 9:57:10', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('02/09/2020 15:14:35', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"d4db9949-5aee-4688-9245-2258afae8fee","act":true,"del":false,"sysname":"MAR_B2B_SH_ALL_MULTI","test":false,"dft":false,"hotX":"AMBOS","tagdiv":true,"tagnac":true,"allMasters":["rate_type","distribution_model"],"divs":[],"exccous":[],"codes":[{"key":"CLIENT_TYPE","value":["B2B"]},{"key":"BRAND","value":["MAR"]}],"new":false,"supplierId":"4f293b7d-64b7-4f65-aace-037ce95e1107"}');
Insert into GESCOM.SISTEMA_EXTERNO_CREDENCIALES
   (SEC_CODSEC, SEC_CODPRC, SEC_NOMSEC, SEC_ACTIVO, SEC_TEST, 
    SEC_HOTELX, SEC_CREATED, SEC_UPDATED, SEC_JSON)
 Values
   ('df894026-6b16-49d6-89f5-ef913248127f', '4f293b7d-64b7-4f65-aace-037ce95e1107', 'WIS_B2B2C_SH_ALL_MULTI_PRUEBAHORAS', '1', '0', 
    'AMBOS', TO_DATE('25/08/2020 10:59:58', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('02/09/2020 15:14:35', 'DD/MM/YYYY HH24:MI:SS'), '{"id":"df894026-6b16-49d6-89f5-ef913248127f","act":true,"del":false,"sysname":"WIS_B2B2C_SH_ALL_MULTI_PRUEBAHORAS","test":false,"dft":false,"hotX":"AMBOS","tagdiv":true,"tagnac":true,"others":"PRUEBAHORAS","allMasters":["rate_type","distribution_model"],"divs":[],"exccous":[],"codes":[{"key":"BRAND","value":["WIS"]},{"key":"CLIENT_TYPE","value":["B2B2C"]}],"new":false,"supplierId":"4f293b7d-64b7-4f65-aace-037ce95e1107"}');
COMMIT;
