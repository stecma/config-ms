SET DEFINE OFF;
Insert into GESCOM.CLIENTE_CREDENCIALES
   (CLC_CODCLC, CLC_CODMRR, CLC_CODTSE, CLC_CODCVC, CLC_CODMDD, 
    CLC_CODENT, CLC_CODPET, CLC_JSON, CLC_CREATED, CLC_UPDATED, 
    CLC_ALIAS)
 Values
   ('9ff17c29-499b-4441-ae2e-f231e9fbaff0', 'MAR', 'HTL', 'WEB', 'BTA', 
    'NVIP', 'MONO', '{"id":"9ff17c29-499b-4441-ae2e-f231e9fbaff0","desc":"Pending","act":false,"del":false,"alias":"Pending","test":false,"demo":false,"xsell":false,"tagnac":true,"hope":false,"hotelX":true,"availcost":false,"priceChange":false,"codes":[{"key":"AGENCY","value":["AFTOU"]},{"key":"DISTRIBUTION_MODEL","value":["BTA"]},{"key":"DEPTH","value":["MARSOL"]},{"key":"ENVIRONMENTS","value":["NVIP"]},{"key":"REQUEST","value":["MONO"]},{"key":"PLATFORM","value":["OFIB","KOE","PUR","DIO1"]},{"key":"LANGUAGES","value":["ESP"]},{"key":"RATE_TYPE","value":["EMP","INTER","NWW"]},{"key":"BRAND","value":["MAR"]},{"key":"PRODUCT_ORIGIN","value":["PPNWT","DHIS","MAR"]},{"key":"USERS","value":["34064792K"]},{"key":"SERVICE_TYPE","value":["HTL"]},{"key":"SALES_MODEL","value":["X"]},{"key":"SALES_CHANNEL","value":["WEB"]}]}', TO_DATE('10/09/2020 9:02:58', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('26/10/2020 15:02:52', 'DD/MM/YYYY HH24:MI:SS'), 
    'Pending');
Insert into GESCOM.CLIENTE_CREDENCIALES
   (CLC_CODCLC, CLC_CODMRR, CLC_CODTSE, CLC_CODCVC, CLC_CODMDD, 
    CLC_CODENT, CLC_CODPET, CLC_JSON, CLC_CREATED, CLC_UPDATED, 
    CLC_ALIAS)
 Values
   ('96dc6ad9-0812-409d-a4e8-14972cb05599', 'WIS', 'HTL', 'API', 'CFL', 
    'VIP', 'MONO', '{"id":"96dc6ad9-0812-409d-a4e8-14972cb05599","desc":"Pending","act":false,"del":false,"alias":"Pending","test":true,"demo":true,"xsell":true,"tagnac":true,"hope":true,"hotelX":true,"availcost":true,"priceChange":false,"share":150,"codes":[{"key":"SALES_CHANNEL","value":["API"]},{"key":"PLATFORM","value":["PEAK","WASA","NETA","KOE","NEMO","PIPE","TVCO","XTG","PUR","ANI","DIO2","TDO","OFIB","INST","WOO"]},{"key":"SALES_MODEL","value":["N"]},{"key":"RATE_TYPE","value":["INTER","RQ","B2B","VINC","EMP","NRF","NWW"]},{"key":"PRODUCT_ORIGIN","value":["PPNWT","PPPULL","MAR","PULL3","DHIS"]},{"key":"REQUEST","value":["MONO"]},{"key":"LANGUAGES","value":["ESP"]},{"key":"AGENCY","value":["ABAYA"]},{"key":"BRAND","value":["WIS"]},{"key":"USERS","value":["34064792K"]},{"key":"SERVICE_TYPE","value":["HTL"]},{"key":"DISTRIBUTION_MODEL","value":["CFL"]},{"key":"DEPTH","value":["LATAM"]},{"key":"ENVIRONMENTS","value":["VIP"]}]}', TO_DATE('02/09/2020 10:55:15', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('18/11/2020 15:30:10', 'DD/MM/YYYY HH24:MI:SS'), 
    'Pending');
Insert into GESCOM.CLIENTE_CREDENCIALES
   (CLC_CODCLC, CLC_CODMRR, CLC_CODTSE, CLC_CODCVC, CLC_CODMDD, 
    CLC_CODENT, CLC_CODPET, CLC_JSON, CLC_CREATED, CLC_UPDATED, 
    CLC_ALIAS)
 Values
   ('12911ecd-e085-4e93-b220-ca952486d5f0', 'MAR', 'HTL', 'API', 'BTA', 
    'VIP', 'MULTI', '{"id":"12911ecd-e085-4e93-b220-ca952486d5f0","desc":"Pending","act":false,"del":false,"alias":"Pending","test":true,"demo":false,"xsell":false,"tagnac":false,"hope":true,"hotelX":true,"availcost":true,"priceChange":false,"share":100000,"codes":[{"key":"SALES_CHANNEL","value":["API"]},{"key":"DISTRIBUTION_MODEL","value":["BTA"]},{"key":"DEPTH","value":["MARSOL"]},{"key":"REQUEST","value":["MULTI"]},{"key":"AGENCY","value":["ABCTR","2001","ABCVI"]},{"key":"RATE_TYPE","value":["B2B","EMP","INTER","NRF","RQ","NWW","VINC"]},{"key":"PRODUCT_ORIGIN","value":["PPPULL","PPNWT","MAR"]},{"key":"COUNTRY","value":["ES"]},{"key":"PLATFORM","value":["ANI","DIO2","BEP","DIO1"]},{"key":"LANGUAGES","value":["ESP"]},{"key":"BRAND","value":["MAR"]},{"key":"USERS","value":["34064792K"]},{"key":"SERVICE_TYPE","value":["HTL"]},{"key":"SALES_MODEL","value":["X"]},{"key":"ENVIRONMENTS","value":["VIP"]}]}', TO_DATE('02/09/2020 11:02:21', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('26/10/2020 15:02:11', 'DD/MM/YYYY HH24:MI:SS'), 
    'Pending');
Insert into GESCOM.CLIENTE_CREDENCIALES
   (CLC_CODCLC, CLC_CODMRR, CLC_CODTSE, CLC_CODCVC, CLC_CODMDD, 
    CLC_CODENT, CLC_CODPET, CLC_JSON, CLC_CREATED, CLC_UPDATED, 
    CLC_ALIAS)
 Values
   ('7565d75a-9d32-4dd8-b2fc-a159a3971bcd', 'MAR', 'HTL', 'API', 'BTA', 
    'VIP', 'DEST', '{"id":"7565d75a-9d32-4dd8-b2fc-a159a3971bcd","desc":"Pending","act":false,"del":true,"alias":"Pending","test":true,"demo":true,"xsell":false,"tagnac":true,"hope":false,"hotelX":true,"availcost":true,"priceChange":true,"share":20000,"codes":[{"key":"SALES_CHANNEL","value":["API"]},{"key":"DISTRIBUTION_MODEL","value":["BTA"]},{"key":"AGENCY","value":["ABACT"]},{"key":"DEPTH","value":["MARSOL"]},{"key":"PLATFORM","value":["INST","KOE","NETA"]},{"key":"REQUEST","value":["DEST"]},{"key":"PRODUCT_ORIGIN","value":["PPNWT","MAR"]},{"key":"LANGUAGES","value":["ESP"]},{"key":"BRAND","value":["MAR"]},{"key":"RATE_TYPE","value":["RQ","B2B","EMP"]},{"key":"USERS","value":["34064792K"]},{"key":"SERVICE_TYPE","value":["HTL"]},{"key":"SALES_MODEL","value":["X"]},{"key":"ENVIRONMENTS","value":["VIP"]}]}', TO_DATE('31/08/2020 13:00:52', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('02/09/2020 10:50:21', 'DD/MM/YYYY HH24:MI:SS'), 
    'Pending_del');
COMMIT;
