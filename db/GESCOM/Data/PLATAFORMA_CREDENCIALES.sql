SET DEFINE OFF;
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('TEST', '[TEST] test', '{"id":"TEST","name":[{"key":"ESP","value":"[TEST] test"}],"act":true,"del":true,"entity":"platform","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('INST', '[INST] Innstant Connect', '{"id":"INST","name":[{"key":"ESP","value":"[INST] Innstant Connect"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('PEAK', '[PEAK] Peakwork', '{"id":"PEAK","name":[{"key":"ESP","value":"[PEAK] Peakwork"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('WASA', '[WASA] Wasabi', '{"id":"WASA","name":[{"key":"ESP","value":"[WASA] Wasabi"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('OFIB', '[OFIB] Ofimática', '{"id":"OFIB","name":[{"key":"ESP","value":"[OFIB] Ofimática"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('XTG', '[XTG] Travelgate - XTG', '{"id":"XTG","name":[{"key":"ESP","value":"[XTG] Travelgate - XTG"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('ANI', '[ANI] Anixe', '{"id":"ANI","name":[{"key":"ESP","value":"[ANI] Anixe"}],"act":true,"del":false,"entity":"PLATFORM","allMasters":[],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"addrem":false,"sendrem":false,"shwrate":false,"supByZone":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('BEP', '[BEP] BePro', '{"id":"BEP","name":[{"key":"ESP","value":"[BEP] BePro"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('DIO1', '[DIO1] Diotur 1', '{"id":"DIO1","name":[{"key":"ESP","value":"[DIO1] Diotur 1"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('DIO2', '[DIO2] Diotur 2', '{"id":"DIO2","name":[{"key":"ESP","value":"[DIO2] Diotur 2"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('KOE', '[KOE] Koedia', '{"id":"KOE","name":[{"key":"ESP","value":"[KOE] Koedia"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('NEMO', '[NEMO] Nemo/Pricesurfer', '{"id":"NEMO","name":[{"key":"ESP","value":"[NEMO] Nemo/Pricesurfer"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('NETA', '[NETA] Netactica', '{"id":"NETA","name":[{"key":"ESP","value":"[NETA] Netactica"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('PIPE', '[PIPE] Pipelie', '{"id":"PIPE","name":[{"key":"ESP","value":"[PIPE] Pipelie"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('PUR', '[PUR] Pursuit', '{"id":"PUR","name":[{"key":"ESP","value":"[PUR] Pursuit"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('TDO', '[TDO] Top Dog', '{"id":"TDO","name":[{"key":"ESP","value":"[TDO] Top Dog"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('TVCO', '[TVCO] TravelCompositor', '{"id":"TVCO","name":[{"key":"ESP","value":"[TVCO] TravelCompositor"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.PLATAFORMA_CREDENCIALES
   (PLT_CODPLT, PLT_NOMPLT, PLT_JSON)
 Values
   ('WOO', '[WOO] Wooba', '{"id":"WOO","name":[{"key":"ESP","value":"[WOO] Wooba"}],"act":true,"del":false,"entity":"PLATFORM","masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
COMMIT;
