SET DEFINE OFF;
Insert into GESCOM.TIPO_SERVICIO_CREDENCIALES
   (TSE_CODTSE, TSE_NOMTSE, TSE_JSON)
 Values
   ('ALEX', '[ALEX] Alex', '{"id":"ALEX","name":[{"key":"ESP","value":"[ALEX] Alex"}],"act":true,"del":false,"entity":"SERVICE_TYPE","allMasters":["sales_model"],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["B2B"]},{"key":"PRODUCT_ORIGIN","value":["PULL3"]},{"key":"PLATFORM","value":["NETA","KOE"]}],"addrem":false,"sendrem":false,"shwrate":false,"supByZone":false}');
Insert into GESCOM.TIPO_SERVICIO_CREDENCIALES
   (TSE_CODTSE, TSE_NOMTSE, TSE_JSON)
 Values
   ('TRF', '[TRF] Traslados', '{"id":"TRF","name":[{"key":"ESP","value":"[TRF] Traslados"}],"act":true,"del":false,"entity":"SERVICE_TYPE","allMasters":["platform"],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC","RQ","NWW"]},{"key":"SALES_MODEL","value":["N","X","C"]},{"key":"PRODUCT_ORIGIN","value":["MAR","PPNWT","PPPULL","PULL3"]}]}');
Insert into GESCOM.TIPO_SERVICIO_CREDENCIALES
   (TSE_CODTSE, TSE_NOMTSE, TSE_JSON)
 Values
   ('HTL', '[HTL] Hotel', '{"id":"HTL","name":[{"key":"ESP","value":"[HTL] Hotel"}],"act":true,"del":false,"entity":"SERVICE_TYPE","allMasters":["rate_type","product_origin","platform"],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"SALES_MODEL","value":["C","N","X"]}],"addrem":false,"sendrem":false,"shwrate":false,"supByZone":false}');
Insert into GESCOM.TIPO_SERVICIO_CREDENCIALES
   (TSE_CODTSE, TSE_NOMTSE, TSE_JSON)
 Values
   ('ALEX2', '[ALEX2] Alex2', '{"id":"ALEX2","name":[{"key":"ESP","value":"[ALEX2] Alex2"}],"act":false,"del":false,"entity":"SERVICE_TYPE","allMasters":["rate_type","sales_model","product_origin","platform"],"masters":[],"test":false,"demo":false,"corp":false,"supplier":false,"own":false}');
Insert into GESCOM.TIPO_SERVICIO_CREDENCIALES
   (TSE_CODTSE, TSE_NOMTSE, TSE_JSON)
 Values
   ('EXP', '[EXP] Experiencias', '{"id":"EXP","name":[{"key":"ESP","value":"[EXP] Experiencias"}],"act":true,"del":false,"entity":"SERVICE_TYPE","allMasters":["sales_model","product_origin","platform"],"test":false,"demo":false,"corp":false,"supplier":false,"own":false,"codes":[{"key":"RATE_TYPE","value":["VINC","RQ","NWW"]}]}');
COMMIT;
