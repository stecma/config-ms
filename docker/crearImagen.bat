@echo off
MODE con:cols=80 lines=40
set /p version=Indica la versión de la imagen:
:inicio
SET var=0
cls
echo ------------------------------------------------------------------------------
echo  Versión de la imagen: %version%
echo ------------------------------------------------------------------------------
echo  1    proxy-ms 
echo  2    login-ms  
echo  3    login-data-ms  
echo  4    gestcom-ms  
echo  5    gestcom-data-ms 
echo  6    Salir
echo ------------------------------------------------------------------------------
echo.

SET /p var= ^> Seleccione una opcion [1-6]:

if "%var%"=="0" goto inicio
if "%var%"=="1" goto proxy-ms
if "%var%"=="2" goto login-ms
if "%var%"=="3" goto login-data-ms
if "%var%"=="4" goto gestcom-ms
if "%var%"=="5" goto gestcom-data-ms
if "%var%"=="6" goto salir

::Mensaje de error, validación cuando se selecciona una opción fuera de rango
echo. El numero "%var%" no es una opcion valida, por favor intente de nuevo.
echo.
pause
echo.
goto:inicio

:proxy-ms
    echo.
		set workspace_location="C:/Globalia/workspace/proxy-ms"
		set docker_file_location="C:/Globalia/docker/dockerfiles/proxy-ms"
		set original_jar=proxy-%version%-exec.jar
		set image_name="proxy-ms"
		goto:crear_imagen
:login-ms
    echo.
		set workspace_location="C:/Globalia/workspace/login-ms"
		set docker_file_location="C:/Globalia/docker/dockerfiles/login-ms"
		set original_jar=login-%version%-exec.jar
		set image_name="login-ms"
		goto:crear_imagen

:login-data-ms
    echo.
		set workspace_location="C:/Globalia/workspace/login-data-ms"
		set docker_file_location="C:/Globalia/docker/dockerfiles/login-data-ms"
		set original_jar=login-data-%version%-exec.jar
		set image_name="login-data-ms"
		goto:crear_imagen
  
:gestcom-ms
    echo.
		set workspace_location="C:/Globalia/workspace/gestcom-ms"
		set docker_file_location="C:/Globalia/docker/dockerfiles/gestcom-ms"
		set original_jar=gestcom-ms-%version%-exec.jar
		set image_name="gestcom-ms"
		goto:crear_imagen

:gestcom-data-ms
    echo.
		set workspace_location="C:/Globalia/workspace/gestcom-data-ms"
		set docker_file_location="C:/Globalia/docker/dockerfiles/gestcom-data-ms"
		set original_jar=gestcom-data-%version%-exec.jar
		set image_name="gestcom-data-ms"
		goto:crear_imagen

:salir
    @cls&exit
	
:crear_imagen
	echo ------- INICIO CREACION: %image_name%:%version% ------- 
	rmdir /Q /S %docker_file_location%\\libs
	mkdir %docker_file_location%\\libs
	cd %workspace_location%
	call mvn clean install -Plocal
	robocopy %workspace_location%\\target %docker_file_location%\\libs /E
	cd %docker_file_location%\\libs
	ren %original_jar% %image_name%.jar
	cd %docker_file_location%
	docker build -t %image_name%:%version% .
	rmdir /Q /S %docker_file_location%\\libs
	echo ------- FIN CREACION: %image_name%:%version% ------- 
	echo.
	pause
	goto:inicio