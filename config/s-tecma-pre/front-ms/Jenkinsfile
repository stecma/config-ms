import groovy.json.JsonSlurper
import groovy.json.JsonOutput

def skipRemainingStages = false

pipeline {
	agent any

	environment {
		EMAIL_RECIPIENTS = 'stecma@globalia-corp.com'
		NEW_VERSION = ""
		GIT_BRANCH = ""
		OPENSHIFT_API = "https://api.dev.gsisservices.com:6443/apis/build.openshift.io/v1/namespaces"
	}

	stages {
		stage('Clone Git') {
			steps {
				script {
					echo 'Clone repository'
					def scmVars = checkout([$class: 'GitSCM',
											branches: [[name: '*/release']],
											doGenerateSubmoduleConfigurations: false,
											extensions: [[$class: 'CloneOption', honorRefspec: true, noTags: true, reference: '', shallow: true]],
											submoduleCfg: [],
											userRemoteConfigs: [[credentialsId: 'SCMCredentials', url: 'https://git.globalia.com/stash/scm/nwt/front-ms.git']]])
					GIT_BRANCH = scmVars.GIT_BRANCH.substring(scmVars.GIT_BRANCH.lastIndexOf("/") + 1)
				}
			}
		}

		stage('Dependencies') {
		    steps {
		        sh 'npm install --loglevel verbose'
		    }
		}

		stage('Docker Build & Push') {
			steps {
				script {
					echo "Docker build & push"
					withCredentials([[$class: 'StringBinding', credentialsId: 'OpenShiftAPITokenRelease', variable: 'BEARER']]) {
						def apiUrl = "${OPENSHIFT_API}/s-tecma-pre/buildconfigs/front-ms/instantiate"
						def headers = "-H \"Authorization: Bearer ${BEARER}\" -H \"Accept: application/json\" -H \"Content-Type: application/json\""
						def curlRQ = """
								curl -k -v -w "%{http_code}" -X POST \
								-d '{"kind":"BuildRequest","apiVersion":"build.openshift.io/v1","metadata":{"name":"front-ms"}}' \
								${headers} ${apiUrl}"""

						def response = sh(returnStdout: true, script: "${curlRQ}").trim()
						def httpCode = response.substring(response.lastIndexOf("}") + 1)
						if (httpCode != "200" && httpCode != "201") {
							skipRemainingStages = true
							error "This pipeline stops here! ${response.substring(0, response.lastIndexOf('}')+1)}"
						}
					}
				}
			}
		}

		stage("Tag and Push") {
			when {
				expression {
					!skipRemainingStages
				}
			}
			steps {
				script {
					echo "Branch: ${GIT_BRANCH}"
					if  (GIT_BRANCH == 'master') {
						getNewVersion(sh(returnStdout:  true, script: "git tag --sort=-taggerdate | head -n 1").trim())
						echo "Tag and Push: ${NEW_VERSION}"
						withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'SCMCredentials', usernameVariable: 'USER', passwordVariable: 'PASS'],]) {
							sh("git tag -a ${NEW_VERSION} -m '[Jenkins CI] New Tag'")
							sh('git push https://${USER}:${PASS}@git.globalia.com/stash/scm/nwt/front-ms.git --tags')
						}
					}
				}
			}
		}
	}

	post {
		always {
			echo 'I have finished and deleting workspace'
			deleteDir()
		}
		success {
			sendEmail("Successful");
		}
		unstable {
			sendEmail("Unstable");
		}
		failure {
			sendEmail("Failed");
		}
	}
}

def getNewVersion(tag) {
	if ("".equals(tag)) {
		NEW_VERSION = "1.0.0"
	} else {
		int oldVersion = tag.substring(tag.lastIndexOf(".") + 1)
		NEW_VERSION = tag.substring(0, tag.lastIndexOf(".") + 1) + (oldVersion + 1)
	}
}

def sendEmail(status) {
	mail(
		to: "$EMAIL_RECIPIENTS",
		subject: "Build $BUILD_NUMBER - " + status + " (${currentBuild.fullDisplayName})",
		body:"Check task output at ${env.BUILD_URL} to view the results.")
}